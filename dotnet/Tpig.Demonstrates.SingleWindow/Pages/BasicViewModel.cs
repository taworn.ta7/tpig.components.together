﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;

namespace Tpig.Demonstrates.SingleWindow.Pages;

public partial class BasicViewModel : ObservableObject, INotifyPropertyChanged
{
    // back
    [RelayCommand]
    private void Back(object o) => BackDelegate?.Invoke(o);
    public Action<object>? BackDelegate { get; set; }

    // next
    [RelayCommand]
    private void Next(object o) => NextDelegate?.Invoke(o);
    public Action<object>? NextDelegate { get; set; }

    // center
    [RelayCommand]
    private void Center(object o) => CenterDelegate?.Invoke(o);
    public Action<object>? CenterDelegate { get; set; }
}
