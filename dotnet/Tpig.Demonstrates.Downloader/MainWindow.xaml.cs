﻿using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Tpig.Components.Localization;
using Tpig.Components.TogetherUi;

namespace Tpig.Demonstrates.Downloader;

/// <summary>
/// Interaction logic for MainWindow.xaml
/// </summary>
public partial class MainWindow : Window
{
    private MainViewModel? viewModel;

    public MainWindow()
    {
        InitializeComponent();

        LocaleManager.Instance.Current = "en";
        Localize.Load(this);

        Loaded += (sender, e) =>
        {
            viewModel = new MainViewModel();
            //viewModel!.ChangeLocaleDelegate += ChangeLocaleDelegate;
            DataContext = viewModel;
        };

        Unloaded += (sender, e) =>
        {
            DataContext = null;
            //viewModel!.ChangeLocaleDelegate -= ChangeLocaleDelegate;
            viewModel = null;
        };
    }

    // ----------------------------------------------------------------------

    private void ChangeLocaleDelegate(object o)
    {
        var locale = o as string;
        LocaleManager.Instance.Current = locale!;
    }

    // ----------------------------------------------------------------------

}
