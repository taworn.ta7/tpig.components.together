﻿using System.Windows.Controls;
using Microsoft.Xaml.Behaviors;

namespace Tpig.Components.TogetherUi;

public class NumberBehavior : Behavior<TextBox>
{
    protected override void OnAttached()
    {
        base.OnAttached();
        AssociatedObject.TextChanged += TextChanged;
    }

    protected override void OnDetaching()
    {
        AssociatedObject.TextChanged -= TextChanged;
        base.OnDetaching();
    }

    private void TextChanged(object sender, TextChangedEventArgs e)
    {
        var caretIndex = AssociatedObject.CaretIndex;
        var text = AssociatedObject.Text;
        while (text.StartsWith("00"))
        {
            text = text.Remove(0, 1);
        }
        if (text.StartsWith("0") && text.Length > 1)
        {
            text = text.Remove(0, 1);
        }
        AssociatedObject.Text = text;
        AssociatedObject.CaretIndex = caretIndex;
    }
}
