﻿namespace Tpig.Components.Together;

/// <summary>
/// After downloaded complete result.
/// </summary>
public enum DownloadedStatusType
{
    Ok,
    TimeOut,
    Error
}


/// <summary>
/// Queue to download.
/// </summary>
public class Queue
{
    /// <summary>
    /// URL to download file from.
    /// </summary>
    public string? Url { get; set; }

    /// <summary>
    /// Local file to push downloaded file.
    /// </summary>
    public string? FileName { get; set; }

    /// <summary>
    /// Callback after download finished or error.
    /// </summary>
    public delegate void CallbackType(Queue q, DownloadedStatusType status);
    public CallbackType? Callback { get; set; }

    /// <summary>
    /// Callback while downloading to display current status.
    /// </summary>
    public delegate void ProgressType(Queue q, long totalBytesToReceive, long bytesReceived, int progressPercentage);
    public ProgressType? Progress { get; set; }

    /// <summary>
    /// A user data.
    /// </summary>
    public object? UserData { get; set; }
}
