﻿using System;
using System.Windows;
using System.Windows.Media.Animation;

namespace Tpig.Components.TogetherUi;

public static partial class Animate
{
    public static void SlideRightToLeft(
        FrameworkElement element0, FrameworkElement element1, Duration timeUsed,
        EventHandler? handler0 = null, EventHandler? handler1 = null)
    {
        var easing = new SineEase() { EasingMode = EasingMode.EaseInOut };
        var a = new ThicknessAnimation()
        {
            From = new Thickness(0, 0, 0, 0),
            To = new Thickness(-element0.ActualWidth, 0, element0.ActualWidth, 0),
            Duration = timeUsed,
            EasingFunction = easing
        };
        var b = new ThicknessAnimation()
        {
            From = new Thickness(element1.ActualWidth, 0, -element1.ActualWidth, 0),
            To = new Thickness(0, 0, 0, 0),
            Duration = timeUsed,
            EasingFunction = easing
        };
        if (handler0 != null)
            a.Completed += handler0;
        if (handler1 != null)
            b.Completed += handler1;
        element0.BeginAnimation(FrameworkElement.MarginProperty, a);
        element1.BeginAnimation(FrameworkElement.MarginProperty, b);
    }

    // ----------------------------------------------------------------------

    public static void SlideLeftToRight(
        FrameworkElement element0, FrameworkElement element1, Duration timeUsed,
        EventHandler? handler0 = null, EventHandler? handler1 = null)
    {
        var easing = new SineEase() { EasingMode = EasingMode.EaseInOut };
        var a = new ThicknessAnimation()
        {
            From = new Thickness(0, 0, 0, 0),
            To = new Thickness(element0.ActualWidth, 0, -element0.ActualWidth, 0),
            Duration = timeUsed,
            EasingFunction = easing
        };
        var b = new ThicknessAnimation()
        {
            From = new Thickness(-element1.ActualWidth, 0, element1.ActualWidth, 0),
            To = new Thickness(0, 0, 0, 0),
            Duration = timeUsed,
            EasingFunction = easing
        };
        if (handler0 != null)
            a.Completed += handler0;
        if (handler1 != null)
            b.Completed += handler1;
        element0.BeginAnimation(FrameworkElement.MarginProperty, a);
        element1.BeginAnimation(FrameworkElement.MarginProperty, b);
    }

    // ----------------------------------------------------------------------

    public static void SlideBottomToTop(
        FrameworkElement element0, FrameworkElement element1, Duration timeUsed,
        EventHandler? handler0 = null, EventHandler? handler1 = null)
    {
        var easing = new SineEase() { EasingMode = EasingMode.EaseInOut };
        var a = new ThicknessAnimation()
        {
            From = new Thickness(0, 0, 0, 0),
            To = new Thickness(0, -element0.ActualHeight, 0, element0.ActualHeight),
            Duration = timeUsed,
            EasingFunction = easing
        };
        var b = new ThicknessAnimation()
        {
            From = new Thickness(0, element1.ActualHeight, 0, -element1.ActualHeight),
            To = new Thickness(0, 0, 0, 0),
            Duration = timeUsed,
            EasingFunction = easing
        };
        if (handler0 != null)
            a.Completed += handler0;
        if (handler1 != null)
            b.Completed += handler1;
        element0.BeginAnimation(FrameworkElement.MarginProperty, a);
        element1.BeginAnimation(FrameworkElement.MarginProperty, b);
    }

    // ----------------------------------------------------------------------

    public static void SlideTopToBottom(
        FrameworkElement element0, FrameworkElement element1, Duration timeUsed,
        EventHandler? handler0 = null, EventHandler? handler1 = null)
    {
        var easing = new SineEase() { EasingMode = EasingMode.EaseInOut };
        var a = new ThicknessAnimation()
        {
            From = new Thickness(0, 0, 0, 0),
            To = new Thickness(0, element0.ActualHeight, 0, -element0.ActualHeight),
            Duration = timeUsed,
            EasingFunction = easing
        };
        var b = new ThicknessAnimation()
        {
            From = new Thickness(0, -element1.ActualHeight, 0, element1.ActualHeight),
            To = new Thickness(0, 0, 0, 0),
            Duration = timeUsed,
            EasingFunction = easing
        };
        if (handler0 != null)
            a.Completed += handler0;
        if (handler1 != null)
            b.Completed += handler1;
        element0.BeginAnimation(FrameworkElement.MarginProperty, a);
        element1.BeginAnimation(FrameworkElement.MarginProperty, b);
    }
}
