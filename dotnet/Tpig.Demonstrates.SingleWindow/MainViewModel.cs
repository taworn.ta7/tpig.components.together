﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using Tpig.Components.TogetherUi;

namespace Tpig.Demonstrates.SingleWindow;

public partial class MainViewModel : ObservableObject, INotifyPropertyChanged
{
    // change locale
    [RelayCommand]
    private void ChangeLocale(object o) => ChangeLocaleDelegate?.Invoke(o);
    public Action<object>? ChangeLocaleDelegate { get; set; }
}
