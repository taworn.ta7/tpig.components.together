﻿using System.Runtime.InteropServices;

namespace Tpig.Components.Together;

public static partial class Helpers
{
    /// <summary>
    /// Thank you https://stackoverflow.com/questions/2871/reading-a-c-c-data-structure-in-c-sharp-from-a-byte-array
    /// and these users:
    /// - https://stackoverflow.com/users/42/coincoin
    /// - https://stackoverflow.com/users/424129/ed-plunkett
    /// - https://stackoverflow.com/users/184528/cdiggins
    /// </summary>
    public static T ByteArrayToStructure<T>(byte[] data) where T : struct
    {
        var handle = GCHandle.Alloc(data, GCHandleType.Pinned);
        try
        {
            return (T)Marshal.PtrToStructure(handle.AddrOfPinnedObject(), typeof(T))!;
        }
        finally
        {
            handle.Free();
        }
    }


    /// <summary>
    /// Thank you https://stackoverflow.com/questions/3278827/how-to-convert-a-structure-to-a-byte-array-in-c
    /// and these users:
    /// - https://stackoverflow.com/users/16299/vincent-mcnabb
    /// - https://stackoverflow.com/users/185655/abdel-raoof
    /// </summary>
    public static byte[] StructureToByteArray<T>(T data) where T : struct
    {
        var length = Marshal.SizeOf(data);
        var array = new byte[length];
        var ptr = Marshal.AllocHGlobal(length);
        Marshal.StructureToPtr(data, ptr, true);
        Marshal.Copy(ptr, array, 0, length);
        Marshal.FreeHGlobal(ptr);
        return array;
    }
}
