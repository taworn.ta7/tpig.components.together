﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Tpig.Components.TogetherUi;

public class SwitchImageButton : Button
{
    static SwitchImageButton()
    {
        DefaultStyleKeyProperty.OverrideMetadata(typeof(SwitchImageButton), new FrameworkPropertyMetadata(typeof(SwitchImageButton)));
    }

    public static readonly DependencyProperty StretchProperty = DependencyProperty.Register("Stretch", typeof(Stretch), typeof(SwitchImageButton), new UIPropertyMetadata(Stretch.Fill));

    public Stretch Stretch
    {
        get { return (Stretch)GetValue(StretchProperty); }
        set { SetValue(StretchProperty, value); }
    }

    public static readonly DependencyProperty NormalImageProperty = DependencyProperty.Register("NormalImage", typeof(ImageSource), typeof(SwitchImageButton), new UIPropertyMetadata(null));
    public static readonly DependencyProperty HoverImageProperty = DependencyProperty.Register("HoverImage", typeof(ImageSource), typeof(SwitchImageButton), new UIPropertyMetadata(null));
    public static readonly DependencyProperty PressImageProperty = DependencyProperty.Register("PressImage", typeof(ImageSource), typeof(SwitchImageButton), new UIPropertyMetadata(null));
    public static readonly DependencyProperty DisableImageProperty = DependencyProperty.Register("DisableImage", typeof(ImageSource), typeof(SwitchImageButton), new UIPropertyMetadata(null));

    public ImageSource NormalImage
    {
        get { return (ImageSource)GetValue(NormalImageProperty); }
        set { SetValue(NormalImageProperty, value); }
    }

    public ImageSource HoverImage
    {
        get { return (ImageSource)GetValue(HoverImageProperty); }
        set { SetValue(HoverImageProperty, value); }
    }

    public ImageSource PressImage
    {
        get { return (ImageSource)GetValue(PressImageProperty); }
        set { SetValue(PressImageProperty, value); }
    }

    public ImageSource DisableImage
    {
        get { return (ImageSource)GetValue(DisableImageProperty); }
        set { SetValue(DisableImageProperty, value); }
    }
}
