﻿using System.Collections.Generic;
using System.Windows;
using Tpig.Interop;

namespace Tpig.Components.TogetherUi;

public static partial class Helpful
{
    public enum ChooseMonitorType
    {
        Biggest = -4,
        Smallest = -3,
        BiggestAlign = -2,
        SmallestAlign = -1,
        Monitor0 = 0,
        Monitor1 = 1,
        Monitor2 = 2,
        Monitor3 = 3
    }


    public enum WindowPositionType
    {
        NoSet = 0,
        LeftTop = 1,
        CenterTop = 2,
        RightTop = 3,
        LeftCenter = 4,
        CenterCenter = 5,
        RightCenter = 6,
        LeftBottom = 7,
        CenterBottom = 8,
        RightBottom = 9,
    }


    /// <summary>
    /// Decide to choose monitor to use, with parameters.
    /// </summary>
    public static Win32.MonitorInfo? ChooseMonitorToDisplay(Window window, List<Win32.MonitorInfo> monitorList, ChooseMonitorType monitorType, bool subtractTaskbarSize)
    {
        var found = (Win32.MonitorInfo?)null;

        if (monitorType >= 0)
        {
            // check if monitor count is ok, otherwise, reset monitor to first one
            var monitor = (int)monitorType;
            if (monitor >= monitorList.Count)
                monitor = 0;
            found = monitorList[monitor];
        }
        else
        {
            var value = 0d;
            if (monitorType == ChooseMonitorType.Biggest)
            {
                // choose biggest monitor
                foreach (var item in monitorList)
                {
                    var r = subtractTaskbarSize ? item.WorkArea : item.Monitor;
                    var x = r.Width * r.Height;
                    if (value <= 0 || value < x)
                    {
                        value = x;
                        found = item;
                    }
                }
            }
            else if (monitorType == ChooseMonitorType.Smallest)
            {
                // choose smallest monitor
                foreach (var item in monitorList)
                {
                    var r = subtractTaskbarSize ? item.WorkArea : item.Monitor;
                    var x = r.Width * r.Height;
                    if (value <= 0 || value > x)
                    {
                        value = x;
                        found = item;
                    }
                }
            }
            else if (monitorType == ChooseMonitorType.BiggestAlign)
            {
                // choose biggest align monitor
                var widthOrHeight = window.Width > window.Height;
                foreach (var item in monitorList)
                {
                    var r = subtractTaskbarSize ? item.WorkArea : item.Monitor;
                    var x = widthOrHeight ? r.Width : r.Height;
                    if (value <= 0 || value < x)
                    {
                        value = x;
                        found = item;
                    }
                }
            }
            else if (monitorType == ChooseMonitorType.SmallestAlign)
            {
                // choose smallest align monitor
                var widthOrHeight = window.Width > window.Height;
                foreach (var item in monitorList)
                {
                    var r = subtractTaskbarSize ? item.WorkArea : item.Monitor;
                    var x = widthOrHeight ? r.Width : r.Height;
                    if (value <= 0 || value > x)
                    {
                        value = x;
                        found = item;
                    }
                }
            }
        }

        return found;
    }


    /// <summary>
    /// Move and resize to fit chosen window.
    /// </summary>
    public static void MoveWindowToChosenMonitor(Window window, Rect monitorRect, bool expandToFit)
    {
        if (!expandToFit)
        {
            window.Left += monitorRect.Left;
            window.Top += monitorRect.Top;
        }
        else
        {
            window.Width = monitorRect.Width;
            window.Height = monitorRect.Height;
            window.Left = monitorRect.Left;
            window.Top = monitorRect.Top;
            window.WindowState = WindowState.Maximized;
        }
    }


    /// <summary>
    /// Resize window if needed, both shrink or expand size.
    /// </summary>
    public static void ResizeAndMoveWindow(Window window, Size desireSize, Rect monitorRect, bool shrinkSizeIfNeed, bool expandSizeIfNeed, WindowPositionType windowPosition)
    {
        // reduce/inflate size window, if display is smaller or bigger
        var w = desireSize.Width;
        var h = desireSize.Height;
        var ratio = w / h;
        if (shrinkSizeIfNeed)
        {
            if (w > monitorRect.Width)
            {
                var d = w - monitorRect.Width;
                h -= d / ratio;
                w = monitorRect.Width;
            }
            if (h > monitorRect.Height)
            {
                var d = h - monitorRect.Height;
                w -= d * ratio;
                h = monitorRect.Height;
            }
        }
        if (expandSizeIfNeed)
        {
            var dw = monitorRect.Width - w;
            var dh = monitorRect.Height - h;
            if (dw > 0 && dw <= dh)
            {
                var d = monitorRect.Width - w;
                h += d / ratio;
                w = monitorRect.Width;
            }
            if (dh > 0 && dh <= dw)
            {
                var d = monitorRect.Height - h;
                w += d * ratio;
                h = monitorRect.Height;
            }
        }
        window.Width = w;
        window.Height = h;

        // move to position
        switch (windowPosition)
        {
            case WindowPositionType.LeftTop:
                window.Left = monitorRect.Left;
                window.Top = monitorRect.Top;
                break;
            case WindowPositionType.CenterTop:
                window.Left = (monitorRect.Left + monitorRect.Right) / 2 - w / 2;
                window.Top = monitorRect.Top;
                break;
            case WindowPositionType.RightTop:
                window.Left = monitorRect.Right - w;
                window.Top = monitorRect.Top;
                break;

            case WindowPositionType.LeftCenter:
                window.Left = monitorRect.Left;
                window.Top = (monitorRect.Top + monitorRect.Bottom) / 2 - h / 2;
                break;
            case WindowPositionType.CenterCenter:
                window.Left = (monitorRect.Left + monitorRect.Right) / 2 - w / 2;
                window.Top = (monitorRect.Top + monitorRect.Bottom) / 2 - h / 2;
                break;
            case WindowPositionType.RightCenter:
                window.Left = monitorRect.Right - w;
                window.Top = (monitorRect.Top + monitorRect.Bottom) / 2 - h / 2;
                break;

            case WindowPositionType.LeftBottom:
                window.Left = monitorRect.Left;
                window.Top = monitorRect.Bottom - h;
                break;
            case WindowPositionType.CenterBottom:
                window.Left = (monitorRect.Left + monitorRect.Right) / 2 - w / 2;
                window.Top = monitorRect.Bottom - h;
                break;
            case WindowPositionType.RightBottom:
                window.Left = monitorRect.Right - w;
                window.Top = monitorRect.Bottom - h;
                break;
        }
    }
}
