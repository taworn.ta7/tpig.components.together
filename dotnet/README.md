# Tpig.Components.Together and Tpig.Components.TogetherUi, .NET Version

It's my work on C# .NET WPF and make libraries to reduce my time.  Later, I decide to merge these libraries:

* [Audio](https://gitlab.com/taworn.ta7/tpig.legacy/-/tree/main/tpig.components.audios)
* [CustomUi](https://gitlab.com/taworn.ta7/tpig.legacy/-/tree/main/tpig.components.customui)
* [Navigators, change name to Single Window](https://gitlab.com/taworn.ta7/tpig.legacy/-/tree/main/tpig.components.navigators)
* [Soft Keyboards](https://gitlab.com/taworn.ta7/tpig.legacy/-/tree/main/tpig.components.softkeyboards)
* [Generic Helpers](https://gitlab.com/taworn.ta7/tpig.legacy/-/tree/main/tpig.helpers)
* [Expanded Dialog Boxes and SceneTimer, which used with Single Window](https://gitlab.com/taworn.ta7/tpig.legacy/-/tree/main/tpig.together)

also, I changed code to fix null warning.

Due to [NuGet](https://www.nuget.org) has limited README feature.  You should read in [my repository](https://gitlab.com/taworn.ta7/tpig.components.together).


## Libraries

* [Together Codes](./Documentation/README-Together.md)

* [Together UI](./Documentation/README-TogetherUi.md)


## Last

Sorry, but I'm not good at English. T_T

