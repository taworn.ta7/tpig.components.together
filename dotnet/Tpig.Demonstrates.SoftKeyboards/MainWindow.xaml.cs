﻿using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Tpig.Components.TogetherUi;

namespace Tpig.Demonstrates.SoftKeyboards
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            Loaded += (sender, e) => {
                Dispatcher.BeginInvoke(DispatcherPriority.Normal, (Action)(() => {
                    SoftKeyboardControl.AddThaiLanguage();
                }));
            };
        }
    }
}
