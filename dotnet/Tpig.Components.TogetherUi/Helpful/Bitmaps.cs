﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Media.Imaging;

namespace Tpig.Components.TogetherUi;

public static partial class Helpful
{
    public static BitmapSource? BitmapToBitmapSource(Bitmap bitmap)
    {
        if (bitmap != null)
        {
            using (var memory = new MemoryStream())
            {
                bitmap.Save(memory, ImageFormat.Png);
                memory.Position = 0;
                var image = new BitmapImage();
                image.BeginInit();
                image.StreamSource = memory;
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.EndInit();
                return image;
            }
        }
        else
            return null;
    }

    public static Bitmap? BitmapSourceToBitmap(BitmapSource image)
    {
        if (image != null)
        {
            using (var memory = new MemoryStream())
            {
                var encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(image));
                encoder.Save(memory);
                return new Bitmap(memory);
            }
        }
        else
            return null;
    }

    public static string? BitmapToBase64(Bitmap bitmap, ImageFormat format)
    {
        if (bitmap != null)
        {
            using (var memory = new MemoryStream())
            {
                bitmap.Save(memory, format);
                return Convert.ToBase64String(memory.ToArray());
            }
        }
        else
            return null;
    }

    public static Bitmap? Base64ToBitmap(string base64)
    {
        if (base64 != null)
        {
            var b = Convert.FromBase64String(base64);
            var image = null as Image;
            using (var memory = new MemoryStream(b))
            {
                try
                {
                    image = Image.FromStream(memory);
                }
                catch (Exception)
                {
                    image = null;
                }
            }
            return image as Bitmap;
        }
        else
            return null;
    }
}
