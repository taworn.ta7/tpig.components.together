## Single Window

When I work in my old company.  It not go well to use window.  So, I change to use UserControl and add features:

* Animation, when page switching
* Back and Next
* Scene timer to limit and return first page

and I call these navigator, because the call Back and Next is almost automatic.

I change it name to Single Window, same as SPA == Single Page Application on web, when I want to merge code.

### Feature Changes

From version 0.2.0, it quite change many, here the list:

* ControlNode to Node
* ControlMap to Map
* IWindowExtension to INode
* NavigateMap to Navigation
* Popup feature is remove, see [Dialog Boxes](./README-DialogBoxes.md) and sample to demo
* SceneTimer is merged into Navigation
* When Back and Next, we used string and lookup via Application.Properties, now you can use interface ISingleWindow
* When create UserControl(s), we used to create when use, now it create all UserControl(s) at start, because it's suck when you have to do animate

Please see demo about single window in Tpig.Demonstrates.SingleWindow.

![Screenshot 1](../Screenshots/SingleWindow-0.png) | ![Screenshot 2](../Screenshots/SingleWindow-1.png)
![Screenshot 3](../Screenshots/SingleWindow-2.png) | ![Screenshot 4](../Screenshots/SingleWindow-3.png)
