﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace Tpig.Components.TogetherUi;

public partial class Navigation : ISingleWindow
{
    // tracing
    public Action<string>? TraceDelegate { get; set; }
    public void Trace(string message) => TraceDelegate?.Invoke(message);

    // debugging
    public Action<string>? DebugDelegate { get; set; }
    public void Debug(string message) => DebugDelegate?.Invoke(message);

    // warning
    public Action<string>? WarningDelegate { get; set; }
    public void Warning(string message) => WarningDelegate?.Invoke(message);
}
