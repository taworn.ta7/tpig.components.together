﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using Tpig.Components.Localization;
using Tpig.Components.TogetherUi;

namespace Tpig.Demonstrates.SingleWindow.Pages;

/// <summary>
/// Interaction logic for FirstPage.xaml
/// </summary>
public partial class FirstPage : UserControl, INode
{
    private BasicViewModel? viewModel;

    // single window interface
    private readonly ISingleWindow single;

    // name of this page
    private string name = string.Empty;

    public FirstPage(ISingleWindow single)
    {
        InitializeComponent();
        Localize.Load(this);
        this.single = single;

        Loaded += (sender, e) =>
        {
            viewModel = new BasicViewModel();
            viewModel!.BackDelegate += Back;
            viewModel!.NextDelegate += Next;
            viewModel!.CenterDelegate += Center;
            DataContext = viewModel;
        };

        Unloaded += (sender, e) =>
        {
            DataContext = null;
            viewModel!.CenterDelegate -= Center;
            viewModel!.NextDelegate -= Next;
            viewModel!.BackDelegate -= Back;
            viewModel = null;
        };
    }

    // ----------------------------------------------------------------------

    public void Enter(string name)
    {
        single.Trace(string.Format("{0}: enter", name));
        this.name = name;
    }

    public void Leave(string name)
    {
        single.Trace(string.Format("{0}: leave", name));
        this.name = string.Empty;
    }

    // ----------------------------------------------------------------------

    private void Back(object o)
    {
        var target = o as string;
        single.Trace(string.Format("{0}: back to {1}", this.name, target ?? "default"));
        single.Back(target);
    }

    private void Next(object o)
    {
        var target = o as string;
        single.Trace(string.Format("{0}: next to {1}", this.name, target ?? "default"));
        single.Next(target);
    }

    private void Center(object o)
    {
        try
        {
            single.IsEnabled = false;
            var message = this.Text("Message_First");
            ExpandedMessageWindow.Execute(Window.GetWindow(this), MessageViewModel.DialogSet.InformationSet, message);
        }
        finally
        {
            single.IsEnabled = true;
        }
    }
}
