﻿using System.Runtime.InteropServices;

namespace Tpig.Interop;

public static partial class Win32
{
    [StructLayout(LayoutKind.Sequential)]
    public struct RECT
    {
        public int left;
        public int top;
        public int right;
        public int bottom;
    }
}
