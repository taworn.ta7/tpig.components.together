﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Windows;

namespace Tpig.Interop;

public static partial class Win32
{
    [StructLayout(LayoutKind.Sequential)]
    public struct MONITORINFO
    {
        public int size;
        public RECT monitor;
        public RECT workArea;
        public uint flags;
    }
    public const int MONITORINFOF_PRIMARY = 1;


    [DllImport("user32.dll")]
    private static extern bool EnumDisplayMonitors(IntPtr hdc, IntPtr lprcClip, MonitorEnumDelegate lpfnEnum, IntPtr dwData);
    public delegate bool MonitorEnumDelegate(IntPtr hMonitor, IntPtr hdcMonitor, ref RECT lprcMonitor, IntPtr dwData);


    [DllImport("user32.dll")]
    private static extern bool GetMonitorInfo(IntPtr hMonitor, ref MONITORINFO lpmi);


    public class MonitorInfo
    {
        public int Index { get; set; } = -1;
        public Rect Monitor { get; set; }
        public Rect WorkArea { get; set; }
        public bool IsPrimary { get; set; } = false;

        public override string ToString() =>
            string.Format("Monitor {9}: Rect ({0}, {1}) x ({2}, {3}); WorkArea ({4}, {5}) x ({6}, {7}); Primary={8}",
                Monitor.Left, Monitor.Top, Monitor.Width, Monitor.Height,
                WorkArea.Left, WorkArea.Top, WorkArea.Width, WorkArea.Height,
                IsPrimary, Index);
    }


    public static List<MonitorInfo> GetAllMonitors()
    {
        var list = new List<MonitorInfo>();
        var index = 0;
        EnumDisplayMonitors(IntPtr.Zero, IntPtr.Zero, delegate (IntPtr hMonitor, IntPtr hdcMonitor, ref RECT lprcMonitor, IntPtr dwData)
        {
            var mi = new MONITORINFO();
            mi.size = Marshal.SizeOf(mi);
            var success = GetMonitorInfo(hMonitor, ref mi);
            if (success)
            {
                var m = mi.monitor;
                var w = mi.workArea;
                list.Add(new MonitorInfo
                {
                    Index = index,
                    Monitor = new Rect(m.left, m.top, m.right - m.left, m.bottom - m.top),
                    WorkArea = new Rect(w.left, w.top, w.right - w.left, w.bottom - w.top),
                    IsPrimary = (mi.flags & MONITORINFOF_PRIMARY) == MONITORINFOF_PRIMARY
                });
                index++;
            }
            return true;
        }, IntPtr.Zero);
        return list;
    }
}
