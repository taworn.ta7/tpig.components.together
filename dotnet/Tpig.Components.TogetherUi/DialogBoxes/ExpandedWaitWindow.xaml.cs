﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;

namespace Tpig.Components.TogetherUi;

/// <summary>
/// Interaction logic for ExpandedWaitWindow.xaml
/// </summary>
public partial class ExpandedWaitWindow : Window
{
    /// <summary>
    /// Run long process and open wait dialog.
    /// </summary>
    public static bool Execute(
        Window parent,
        Func<object, bool> runInBackground,
        string? text = null,
        Color? color = null,
        object? parameters = null)
    {
        var window = null as ExpandedWaitWindow;
        try
        {
            window = new ExpandedWaitWindow
            {
                Owner = parent,
                locking = true
            };
            window.ending += window.Ending;
            window.WaitControl.Begin(text, color);
            window.RunAsync(runInBackground, parameters);
            window.ShowDialog();
            //window.ending -= window.Ending;
            return window.result ?? false;
        }
        catch (Exception)
        {
            if (window != null)
            {
                window.locking = false;
                window.Close();
            }
            return false;
        }
    }

    private bool locking = false;
    private bool? result = null;
    private Action? ending = null;

    private ExpandedWaitWindow()
    {
        InitializeComponent();

        var source = null as HwndSource;

        Loaded += (sender, e) =>
        {
            WindowState = Owner.WindowState;
            Left = Owner.Left;
            Top = Owner.Top;
            Width = Owner.Width;
            Height = Owner.Height;

            source = (HwndSource)PresentationSource.FromDependencyObject(this);
            source.AddHook(Hooks.HookWindowProc);
        };

        Unloaded += (sender, e) =>
        {
            if (source != null)
            {
                source.RemoveHook(Hooks.HookWindowProc);
                source.Dispose();
            }
        };

        Closing += ClosingHandler!;
    }

    private void ClosingHandler(object sender, CancelEventArgs e)
    {
        e.Cancel = locking;
    }

    private void Ending() => Dispatcher.Invoke(() =>
    {
        WaitControl.End(() => Close());
    });

    private async void RunAsync(Func<object, bool> runInBackground, object? parameters)
    {
        await Task.Run(() =>
        {
            result = runInBackground?.Invoke(parameters ?? new object());
            locking = false;
            ending?.Invoke();
        });
    }
}
