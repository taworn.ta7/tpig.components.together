﻿using System;
using System.IO;
using System.Windows.Media.Imaging;

namespace Tpig.Components.TogetherUi;

public static class SequenceAnimationExtensions
{
    public static bool Add(this SequenceAnimation animation, string imageFile, TimeSpan? timeOut = null)
    {
        try
        {
            var image = new BitmapImage(new Uri(imageFile));
            animation.Add(image, timeOut);
            return true;
        }
        catch (Exception)
        {
            return false;
        }
    }

    public static bool Add(this SequenceAnimation animation, Uri imageFile, TimeSpan? timeOut = null)
    {
        try
        {
            var image = new BitmapImage(imageFile);
            animation.Add(image, timeOut);
            return true;
        }
        catch (Exception)
        {
            return false;
        }
    }

    public static void AddFolder(this SequenceAnimation animation, string directory)
    {
        directory = Environment.ExpandEnvironmentVariables(directory);

        var files = null as string[];
        try
        {
            files = Directory.GetFiles(directory, "*", SearchOption.TopDirectoryOnly);
        }
        catch (Exception)
        {
            return;
        }

        foreach (var item in files)
        {
            var extension = Path.GetExtension(item).ToLower();
            if (extension == ".png" || extension == ".jpg" || extension == ".jpeg")
            {
                animation.Add(item);
            }
        }
    }
}
