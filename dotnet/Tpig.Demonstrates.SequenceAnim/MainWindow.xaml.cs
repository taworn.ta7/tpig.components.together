﻿using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Tpig.Components.TogetherUi;

namespace Tpig.Demonstrates.SequenceAnim;

/// <summary>
/// Interaction logic for MainWindow.xaml
/// </summary>
public partial class MainWindow : Window
{
    private static SequenceAnimation? sprite = null;

    private SequenceAnimationRenderer? renderer = null;

    public MainWindow()
    {
        InitializeComponent();

        if (sprite == null)
        {
            sprite = new SequenceAnimation
            {
                DefaultTimeOut = TimeSpan.FromMilliseconds(50),
                IsSharedImages = true
            };
            var resPathTemplate = "/Tpig.Demonstrates.SequenceAnim;component/Resources/frame-{0}.png";
            for (var i = 0; i < 20; i++)
            {
                var number = string.Format("{0:00}", i);
                var resPath = string.Format(resPathTemplate, number);
                var image = new BitmapImage(new Uri(resPath, UriKind.RelativeOrAbsolute));
                sprite.Add(image);
            }
        }

        renderer = new SequenceAnimationRenderer(WaitImage)
        {
            Animation = sprite,
            End = SequenceAnimationRenderer.PlayEnd.Repeat
        };

        StartButton.Click += (sender, e) => renderer.Play();
        StopButton.Click += (sender, e) => renderer.Stop();
    }
}
