﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Threading;
using Tpig.Components.Localization;

namespace Tpig.Components.TogetherUi;

/// <summary>
/// Interaction logic for ExpandedMessageWindow.xaml
/// </summary>
public partial class ExpandedMessageWindow : Window
{
    /// <summary>
    /// Open message window, wait and return the result.
    /// </summary>
    public static MessageViewModel.Id Execute(
        Window parent,
        MessageViewModel.DialogSet dialogSet,
        string message,
        string caption = "")
    {
        var vm = new MessageViewModel(dialogSet) { Caption = caption, Message = message };
        var window = new ExpandedMessageWindow(vm) { Owner = parent };
        window.ShowDialog();
        return window.Result;
    }

    /// <summary>
    /// Open message window with customization, wait and return the result.
    /// </summary>
    public static MessageViewModel.Id Execute(Window parent, MessageViewModel vm)
    {
        var window = new ExpandedMessageWindow(vm) { Owner = parent };
        window.ShowDialog();
        return window.Result;
    }

    // ----------------------------------------------------------------------

    private MessageViewModel? viewModel = null;

    private bool locking = false;
    private string? timeLeftFormatter = null;
    private DispatcherTimer? timer = null;
    private DateTime timeStart = DateTime.Now;

    private readonly TimeSpan animateTime = TimeSpan.FromMilliseconds(250);

    public MessageViewModel.Id Result { get; private set; } = 0;

    public ExpandedMessageWindow(MessageViewModel vm)
    {
        InitializeComponent();

        Localize.Load(this);

        var source = null as HwndSource;
        RootPanel.Visibility = Visibility.Hidden;

        Loaded += (sender, e) =>
        {
            WindowState = Owner.WindowState;
            Left = Owner.Left;
            Top = Owner.Top;
            Width = Owner.Width;
            Height = Owner.Height;

            source = (HwndSource)PresentationSource.FromDependencyObject(this);
            source.AddHook(Hooks.HookWindowProc);

            viewModel = vm;
            viewModel!.ClosedDelegate += OnClosed;
            DataContext = viewModel;

            if (viewModel.AutoClose != null)
            {
                timeLeftFormatter = viewModel.AutoClose.Formatter;
                TimeLeftText.Text = string.Format(timeLeftFormatter ?? "", (int)viewModel.AutoClose.TimeOut.TotalMilliseconds / 1000);
                TimeLeftText.Visibility = Visibility.Visible;

                timer = new DispatcherTimer();
                timer.Tick += Tick!;
                timer.Interval = animateTime;
                timeStart = DateTime.Now;

                timer.Start();
            }
            else
            {
                TimeLeftText.Visibility = Visibility.Collapsed;
            }

            locking = true;

            Dispatcher.BeginInvoke(DispatcherPriority.Render, (Action)(() =>
            {
                RootPanel.Visibility = Visibility.Visible;
                Animate.FadeIn(RootPanel, animateTime);
            }));
        };

        Unloaded += (sender, e) =>
        {
            if (timer != null)
            {
                timer.Stop();
                timer = null;
            }

            DataContext = null;
            viewModel!.ClosedDelegate -= OnClosed;
            viewModel = null;

            source!.RemoveHook(Hooks.HookWindowProc);
            source!.Dispose();
        };

        Closing += ClosingHandler!;
    }

    // ----------------------------------------------------------------------

    private void ClosingHandler(object sender, CancelEventArgs e)
    {
        e.Cancel = locking;
        if (!locking)
        {
            if (timer != null)
            {
                timer.Stop();
                timer = null;
            }

            e.Cancel = true;
            IsEnabled = false;
            Animate.FadeOut(this, animateTime, (_0, _1) =>
            {
                Closing -= ClosingHandler!;
                Close();
            });
        }
    }

    private void OnClosed(MessageViewModel.Id id)
    {
        locking = false;
        Result = id;
        DialogResult = true;
    }

    private void Tick(object sender, EventArgs e)
    {
        var t = DateTime.Now - timeStart;
        if (t <= viewModel!.AutoClose?.TimeOut)
        {
            var span = viewModel.AutoClose.TimeOut - t;
            TimeLeftText.Text = string.Format(timeLeftFormatter ?? "", (int)span.TotalMilliseconds / 1000);
        }
        else
        {
            timer!.Stop();
            locking = false;
            Result = viewModel!.AutoClose!.Result;
            DialogResult = true;
        }
    }
}
