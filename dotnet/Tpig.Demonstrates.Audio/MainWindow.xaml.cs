﻿using System.ComponentModel;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Threading;
using NAudio.Wave;
using Tpig.Components.TogetherUi;

namespace Tpig.Demonstrates.Audio;

/// <summary>
/// Interaction logic for MainWindow.xaml
/// </summary>
public partial class MainWindow : Window
{
    private IAudioFile? audioFile = null;

    public MainWindow()
    {
        InitializeComponent();

        Loaded += (sender, e) =>
        {
        };

        Unloaded += (sender, e) =>
        {
        };

        Closed += (sender, e) =>
        {
        };
    }

    // ----------------------------------------------------------------------

    private void OpenAudioClick(object sender, RoutedEventArgs e)
    {
        var dialog = new Microsoft.Win32.OpenFileDialog()
        {
            DefaultExt = ".mp3",
            CheckFileExists = true,
            CheckPathExists = true,
            FileName = string.Empty,
            Filter = "Audio Files (*.mp3;*.aiff;*.aac;*.flac;*.wma;*.wav)|*.mp3;*.aiff;*.aac;*.flac;*.wma;*.wav|All Files (*.*)|*.*"
        };
        var result = dialog.ShowDialog();
        if (result == true)
        {
            try
            {
                var audio = new AudioFile(dialog.FileName);
                if (!audio.CanPlay)
                    throw new Exception(string.Format("Cannot play file: {0}", dialog.FileName));

                if (audioFile != null)
                    audioFile.Dispose();
                audioFile = audio;
                audioFile.Stopped += AudioStopped!;
                audioFile.Play();
                audioFile.IsRepeat = RepeatCheck.IsChecked == true;
                audioFile.Volume = (float)VolumeSlider.Value / 100;

                Dispatcher.BeginInvoke(DispatcherPriority.ApplicationIdle, () => PrintInformation());
            }
            catch (Exception ex)
            {
                var message = string.Format("Cannot open file: {0}", dialog.FileName) + "\n\n" + ex.Message;
                var caption = "Error";
                MessageBox.Show(this, message, caption, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }

    private void PrintInformation()
    {
        InfoText.Text = "";
        if (audioFile == null || !audioFile.CanPlay)
            return;

        var s = string.Format("{0}\n", audioFile.FileName);
        s += string.Format("extension: {0}\n", audioFile.Extension);
        s += string.Format("recognition: {0}\n", audioFile.Format);
        s += "\n";

        if (audioFile.Format == AudioType.Wave)
        {
            using (var reader = new WaveFileReader(audioFile.FileName))
            {
                var w = reader.WaveFormat;
                s += string.Format("WaveFormat\n");
                s += "\n";
                s += string.Format("Encoding:              {0}\n", w.Encoding);
                s += string.Format("Channels:              {0}\n", w.Channels);
                s += string.Format("SampleRate:            {0:#,0} Hz\n", w.SampleRate);
                s += string.Format("AverageBytesPerSecond: {0:#,0} byte(s)\n", w.AverageBytesPerSecond);
                s += string.Format("BlockAlign:            {0:#,0} byte(s)\n", w.BlockAlign);
                s += string.Format("BitsPerSample:         {0:#,0} bit(s)\n", w.BitsPerSample);
                s += string.Format("ExtraSize:             {0:#,0} byte(s)\n", w.ExtraSize);
                s += "\n";
            }
        }

        else if (audioFile.Format == AudioType.Mp3)
        {
            using (var reader = new Mp3FileReader(audioFile.FileName))
            {
                var w = reader.Mp3WaveFormat;
                s += string.Format("Mp3WaveFormat\n");
                s += "\n";
                s += string.Format("Encoding:              {0}\n", w.Encoding);
                s += string.Format("Channels:              {0}\n", w.Channels);
                s += string.Format("SampleRate:            {0:#,0} Hz\n", w.SampleRate);
                s += string.Format("AverageBytesPerSecond: {0:#,0} byte(s)\n", w.AverageBytesPerSecond);
                s += string.Format("BlockAlign:            {0:#,0} byte(s)\n", w.BlockAlign);
                s += string.Format("BitsPerSample:         {0:#,0} bit(s)\n", w.BitsPerSample);
                s += string.Format("ExtraSize:             {0:#,0} byte(s)\n", w.ExtraSize);
                s += string.Format("Id:                    {0}\n", w.id);
                s += string.Format("Flags:                 {0}\n", w.flags);
                s += string.Format("BlockSize:             {0}\n", w.blockSize);
                s += string.Format("FramesPerBlock:        {0}\n", w.framesPerBlock);
                var id3v1 = reader.Id3v1Tag;
                s += string.Format("ID3v1:                 {0}\n", id3v1 != null ? "Yes" : "No");
                var id3v2 = reader.Id3v2Tag;
                s += string.Format("ID3v2:                 {0}\n", id3v2 != null ? "Yes" : "No");
                s += "\n";
            }
        }

        InfoText.Text = s;
    }

    private void AudioStopped(object sender, StoppedEventArgs e)
    {
        if (audioFile != null)
        {
            audioFile.IsRepeat = RepeatCheck.IsChecked == true;
            if (!audioFile.IsRepeat)
                InfoText.Text += "Stopped\n";
        }
    }

    private void OnRepeatChecked(object sender, RoutedEventArgs e)
    {
        if (audioFile != null)
        {
            audioFile.IsRepeat = RepeatCheck.IsChecked == true;
            e.Handled = true;
        }
    }

    private void OnVolumeChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
    {
        if (audioFile != null)
        {
            audioFile.Volume = (float)e.NewValue / 100;
            e.Handled = true;
        }
    }
}
