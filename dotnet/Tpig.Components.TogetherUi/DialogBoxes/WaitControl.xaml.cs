﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace Tpig.Components.TogetherUi;

/// <summary>
/// Interaction logic for WaitControl.xaml
/// </summary>
public partial class WaitControl : UserControl
{
    private readonly TimeSpan animateTime = TimeSpan.FromMilliseconds(300);

    public WaitControl()
    {
        InitializeComponent();
        Visibility = Visibility.Hidden;
    }

    /// <summary>
    /// Set Wait to true to open the control, otherwise, set to false to close the control.
    /// </summary>
    private bool wait = false;
    public bool Wait
    {
        get => wait;
        set
        {
            if (wait != value)
            {
                if (value)
                    Begin();
                else
                    End();
            }
        }
    }

    /// <summary>
    /// Open the control.
    /// </summary>
    public void Begin(string? text = null, Color? color = null)
    {
        if (wait)
            return;
        wait = true;

        BackgroundPanel.Background = new SolidColorBrush(color ?? Color.FromArgb(0x88, 0x88, 0x88, 0x88));
        WaitText.Text = text;
        WaitText.Visibility = text != null ? Visibility.Visible : Visibility.Collapsed;
        Visibility = Visibility.Visible;

        var da = new DoubleAnimation(0, 360, new Duration(TimeSpan.FromSeconds(1)));
        var rt = new RotateTransform();
        WaitImage.RenderTransform = rt;
        WaitImage.RenderTransformOrigin = new Point(0.5, 0.5);
        da.RepeatBehavior = RepeatBehavior.Forever;
        rt.BeginAnimation(RotateTransform.AngleProperty, da);

        Animate.FadeIn(this, animateTime, (_0, _1) =>
        {
        });
    }

    /// <summary>
    /// Close the control.
    /// </summary>
    public void End(Action? ended = null)
    {
        if (!wait)
            return;

        Animate.FadeOut(this, animateTime, (_0, _1) =>
        {
            var rt = new RotateTransform();
            WaitImage.RenderTransform = rt;
            rt.BeginAnimation(RotateTransform.AngleProperty, null);

            Visibility = Visibility.Hidden;
            wait = false;
            ended?.Invoke();
        });
    }
}
