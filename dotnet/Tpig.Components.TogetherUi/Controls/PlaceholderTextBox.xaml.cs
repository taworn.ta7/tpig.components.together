﻿using System.Windows;
using System.Windows.Controls;

namespace Tpig.Components.TogetherUi;

public class PlaceholderTextBox : TextBox
{
    static PlaceholderTextBox()
    {
        DefaultStyleKeyProperty.OverrideMetadata(typeof(PlaceholderTextBox), new FrameworkPropertyMetadata(typeof(PlaceholderTextBox)));
    }

    public static readonly DependencyProperty PlaceholderProperty = DependencyProperty.Register("Placeholder", typeof(string), typeof(PlaceholderTextBox), new FrameworkPropertyMetadata("Input text..."));

    public string Placeholder
    {
        get { return (string)GetValue(PlaceholderProperty); }
        set { SetValue(PlaceholderProperty, value); }
    }

    public static readonly DependencyProperty PlaceholderAlignmentProperty = DependencyProperty.Register("PlaceholderAlignment", typeof(TextAlignment), typeof(PlaceholderTextBox), new FrameworkPropertyMetadata(TextAlignment.Left));

    public TextAlignment PlaceholderAlignment
    {
        get { return (TextAlignment)GetValue(PlaceholderAlignmentProperty); }
        set { SetValue(PlaceholderAlignmentProperty, value); }
    }
}
