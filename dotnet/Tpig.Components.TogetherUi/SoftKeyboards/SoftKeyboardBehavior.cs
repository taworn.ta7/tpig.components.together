﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using Microsoft.Xaml.Behaviors;

namespace Tpig.Components.TogetherUi;

public enum SoftKeyboardEnum
{
    International,
    EnglishOnly,
    Numeric
}


public class SoftKeyboardBehavior : Behavior<Control>
{
    public static readonly DependencyProperty SoftKeyboardTypeProperty =
        DependencyProperty.Register("SoftKeyboardType", typeof(SoftKeyboardEnum), typeof(SoftKeyboardBehavior), new FrameworkPropertyMetadata(SoftKeyboardEnum.International));

    public SoftKeyboardEnum SoftKeyboardType
    {
        get => (SoftKeyboardEnum)GetValue(SoftKeyboardTypeProperty);
        set { SetValue(SoftKeyboardTypeProperty, value); }
    }

    private SoftKeyboardEnum keyboardType = SoftKeyboardEnum.International;
    private SoftKeyboardControl? softKeyboard = null;

    protected override void OnAttached()
    {
        base.OnAttached();
        AssociatedObject.PreviewMouseDown += PreviewMouseDown;
        AssociatedObject.GotFocus += GotFocus;
        AssociatedObject.LostFocus += LostFocus;
        keyboardType = SoftKeyboardType;
    }

    protected override void OnDetaching()
    {
        AssociatedObject.LostFocus -= LostFocus;
        AssociatedObject.GotFocus -= GotFocus;
        AssociatedObject.PreviewMouseDown -= PreviewMouseDown;
        base.OnDetaching();
    }

    private void PreviewMouseDown(object sender, MouseButtonEventArgs e)
    {
        softKeyboard ??= SoftKeyboardUtilities.FindSoftKeyboard(AssociatedObject);
        softKeyboard?.Show(AssociatedObject, keyboardType);
    }

    private void GotFocus(object sender, RoutedEventArgs e)
    {
        softKeyboard ??= SoftKeyboardUtilities.FindSoftKeyboard(AssociatedObject);
        softKeyboard?.Show(AssociatedObject, keyboardType);
    }

    private void LostFocus(object sender, RoutedEventArgs e)
    {
        softKeyboard ??= SoftKeyboardUtilities.FindSoftKeyboard(AssociatedObject);
        softKeyboard?.Hide();
    }

}

// ----------------------------------------------------------------------

public static class AutoSoftKeyboardBehavior
{
    public static bool GetUseSoftKeyboard(DependencyObject d)
    {
        return (bool)d.GetValue(UseSoftKeyboardProperty);
    }

    public static void SetUseSoftKeyboard(DependencyObject d, bool value)
    {
        d.SetValue(UseSoftKeyboardProperty, value);
    }

    public static readonly DependencyProperty UseSoftKeyboardProperty = DependencyProperty.RegisterAttached("UseSoftKeyboard", typeof(bool), typeof(AutoSoftKeyboardBehavior), new PropertyMetadata(false, OnUseSoftKeyboardChanged));

    private static void OnUseSoftKeyboardChanged(object sender, DependencyPropertyChangedEventArgs e)
    {
        if (sender is TextBoxBase)
        {
            var o = sender as TextBoxBase;
            var isUsed = (bool)e.NewValue;
            if (isUsed)
            {
                o!.PreviewMouseDown += PreviewMouseDown;
                o!.GotFocus += GotFocus;
                o!.LostFocus += LostFocus;
            }
            else
            {
                o!.LostFocus -= LostFocus;
                o!.GotFocus -= GotFocus;
                o!.PreviewMouseDown -= PreviewMouseDown;
            }
        }
        else if (sender is PasswordBox)
        {
            var o = sender as PasswordBox;
            var isUsed = (bool)e.NewValue;
            if (isUsed)
            {
                o!.PreviewMouseDown += PreviewMouseDown;
                o!.GotFocus += GotFocus;
                o!.LostFocus += LostFocus;
            }
            else
            {
                o!.LostFocus -= LostFocus;
                o!.GotFocus -= GotFocus;
                o!.PreviewMouseDown -= PreviewMouseDown;
            }
        }
    }

    private static void PreviewMouseDown(object sender, MouseButtonEventArgs e)
    {
        if (sender is TextBoxBase)
        {
            var o = sender as TextBoxBase;
            var softKeyboard = SoftKeyboardUtilities.FindSoftKeyboard(o!);
            softKeyboard?.Show(o!, SoftKeyboardEnum.International);
        }
        else if (sender is PasswordBox)
        {
            var o = sender as PasswordBox;
            var softKeyboard = SoftKeyboardUtilities.FindSoftKeyboard(o!);
            softKeyboard?.Show(o!, SoftKeyboardEnum.International);
        }
    }

    private static void GotFocus(object sender, RoutedEventArgs e)
    {
        if (sender is TextBoxBase)
        {
            var o = sender as TextBoxBase;
            var softKeyboard = SoftKeyboardUtilities.FindSoftKeyboard(o!);
            softKeyboard?.Show(o!, SoftKeyboardEnum.International);
        }
        else if (sender is PasswordBox)
        {
            var o = sender as PasswordBox;
            var softKeyboard = SoftKeyboardUtilities.FindSoftKeyboard(o!);
            softKeyboard?.Show(o!, SoftKeyboardEnum.International);
        }
    }

    private static void LostFocus(object sender, RoutedEventArgs e)
    {
        if (sender is TextBoxBase)
        {
            var o = sender as TextBoxBase;
            var softKeyboard = SoftKeyboardUtilities.FindSoftKeyboard(o!);
            softKeyboard?.Hide();
        }
        else if (sender is PasswordBox)
        {
            var o = sender as PasswordBox;
            var softKeyboard = SoftKeyboardUtilities.FindSoftKeyboard(o!);
            softKeyboard?.Hide();
        }
    }
}
