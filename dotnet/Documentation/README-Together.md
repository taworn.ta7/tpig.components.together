# Tpig.Components.Together

Codes for various parts.

* [Downloader](https://gitlab.com/taworn.ta7/tpig.legacy/-/tree/main/tpig.components.downloaders)




# Features

## Downloader

...




# Release Notes

Please see [CHANGELOG](../Tpig.Components.Together/CHANGELOG.md).




# Thank For These People

* [ByteArrayToStructure](https://stackoverflow.com/questions/2871/reading-a-c-c-data-structure-in-c-sharp-from-a-byte-array)

* [StructureToByteArray](https://stackoverflow.com/questions/3278827/how-to-convert-a-structure-to-a-byte-array-in-c)

* HttpClientEx, based on [HttpClientDownloadWithProgress](https://stackoverflow.com/questions/20661652/progress-bar-with-httpclient), 
answered by [René Sackers](https://stackoverflow.com/users/439094/ren%c3%a9-sackers)




# Last

Sorry, but I'm not good at English. T_T

