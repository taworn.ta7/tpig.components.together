﻿using System;
using System.Threading;
using System.Windows;
using System.Windows.Media;
using Tpig.Components.Localization;
using Tpig.Components.TogetherUi;

namespace Tpig.Demonstrates.ExpandedDialogBoxes
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MainViewModel? viewModel;

        private Timer? timer = null;

        public MainWindow()
        {
            InitializeComponent();
            LocaleManager.Instance.Current = "en";
            Localize.Load(this);

            Loaded += (sender, e) =>
            {
                viewModel = new MainViewModel();
                viewModel.ChangeLocaleDelegate += ChangeLocaleDelegate;
                viewModel.MessageDelegate += MessageDelegate;
                viewModel.WaitDelegate += WaitDelegate;
                DataContext = viewModel;

                var locale = LocaleManager.Instance.Current;
                ChangeLocaleEnButton.IsChecked = ChangeLocaleEnButton.CommandParameter?.ToString()?.Equals(locale);
                ChangeLocaleThButton.IsChecked = ChangeLocaleThButton.CommandParameter?.ToString()?.Equals(locale);
            };

            Unloaded += (sender, e) =>
            {
                if (timer != null)
                {
                    timer.Change(Timeout.Infinite, Timeout.Infinite);
                    timer.Dispose();
                    timer = null;
                }

                DataContext = null;
                if (viewModel != null)
                {
                    viewModel.WaitDelegate -= WaitDelegate;
                    viewModel.MessageDelegate -= MessageDelegate;
                    viewModel.ChangeLocaleDelegate -= ChangeLocaleDelegate;
                }
            };
        }

        // ----------------------------------------------------------------------

        private void AddLog(string log)
        {
            LogText.Text += log + "\n";
        }

        // ----------------------------------------------------------------------

        private void ChangeLocaleDelegate(object o)
        {
            var locale = o as string;
            LocaleManager.Instance.Current = locale ?? "";
            AddLog(string.Format("change locale to: {0}", locale));
        }

        private void MessageDelegate(object o)
        {
            if (o.Equals("Custom"))
            {
                if (viewModel!.DialogSetSelected != null)
                {
                    var result = ExpandedMessageWindow.Execute(
                        this,
                        viewModel.DialogSetSelected.Id,
                        this.Text("Message_Custom"),
                        this.Text("Message_Custom"));
                    AddLog(string.Format("question, result: {0}", result));
                }
            }

            else if (o.Equals("Question"))
            {
                var vm = new MessageViewModel
                {
                    //Caption = this.Text("Message_Question"),
                    Message = this.Text("Message_Question"),
                    Buttons = MessageViewModel.Group.RetryCancel,
                    Decorate = MessageViewModel.Adorn.Warning,
                    AutoClose = new MessageViewModel.AutoCloseTimer
                    {
                        Formatter = this.Text("Message_TimeLeft"),
                        TimeOut = TimeSpan.FromMilliseconds(4999),
                        Result = MessageViewModel.Id.No
                    }
                };
                var result = ExpandedMessageWindow.Execute(this, vm);
                AddLog(string.Format("question, result: {0}", result));
            }

            else if (o.Equals("Info"))
            {
                var caption = this.Text("Message_Info");
                var message = this.Text("Message_Info");
                var result = ExpandedMessageWindow.Execute(this, MessageViewModel.DialogSet.InformationSet, message, caption);
                AddLog(string.Format("information, result: {0}", result));
            }

            else if (o.Equals("Warn"))
            {
                var caption = this.Text("Message_Warn");
                var message = this.Text("Message_Warn");
                var result = ExpandedMessageWindow.Execute(this, MessageViewModel.DialogSet.WarningSet, message, caption);
                AddLog(string.Format("warning, result: {0}", result));
            }
        }

        private void WaitDelegate(object o)
        {
            if (o.Equals("Background"))
            {
                var message = this.Text("Wait_Text");
                var result = ExpandedWaitWindow.Execute(this, (_0) =>
                {
                    Thread.Sleep(1999);
                    return true;
                }, message, Color.FromArgb(0xcc, 0x00, 0x00, 0x00));
                AddLog(string.Format("waiting, return: {0}", result));
            }
            else if (o.Equals("Foreground"))
            {
                var message = this.Text("Wait_Text");
                ExpandedWaitControl.Begin(message, Color.FromArgb(0x88, 0x44, 0x99, 0xcc));
                timer = new Timer((_0) => Dispatcher.Invoke(() =>
                {
                    ExpandedWaitControl.End();
                    timer?.Dispose();
                    timer = null;
                }), null, 1999, 0);
            }
        }
    }
}
