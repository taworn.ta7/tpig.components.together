﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Threading;

namespace Tpig.Components.Together;

/// <summary>
/// Download batch components.
/// </summary>
public class Downloader
{
    /// <summary>
    /// Constructor.
    /// </summary>
    public Downloader()
    {
    }

    /// <summary>
    /// All the downloads is completed or error.
    /// </summary>
    public bool Finished
    {
        get => queueList.Count <= 0 && fetcher == null;
    }

    // ----------------------------------------------------------------------

    // Manage Queue

    private List<Queue> queueList = new List<Queue>();

    public int QueueCount
    {
        get => queueList.Count;
    }

    public void AddToQueue(string url, string fileName, Queue.CallbackType callback, Queue.ProgressType progress = null, bool priority = false)
    {
        var q = new Queue
        {
            Url = url,
            FileName = fileName,
            Callback = callback,
            Progress = progress
        };
        AddToQueue(q, priority);
    }

    public void AddToQueue(Queue q, bool priority = false)
    {
        if (priority)
            queueList.Insert(0, q);
        else
            queueList.Add(q);
    }

    public void Clear()
    {
        queueList.Clear();
    }

    // ----------------------------------------------------------------------

    // Start and Stop Running Downloads

    private Timer timer = null;
    private int CheckInterval = 1000;

    public void Start()
    {
        if (timer == null)
        {
            timer = new Timer(Tick, null, CheckInterval, Timeout.Infinite);
        }
    }

    public void Stop()
    {
        if (timer != null)
        {
            timer.Change(Timeout.Infinite, Timeout.Infinite);
            timer.Dispose();
            timer = null;
        }
    }

    // ----------------------------------------------------------------------

    // Downloading

#if DEBUG
    public TimeSpan TimeOut { get; set; } = TimeSpan.FromSeconds(15);
#else
        public TimeSpan TimeOut { get; set; } = TimeSpan.FromSeconds(25);
#endif

    private WebClient client = null;
    private Fetcher fetcher = null;
    private DateTime timeStart;

    private void Tick(object state)
    {
        if (fetcher != null)
        {
            if (!fetcher.IsCancelled)
            {
                // we are still downloading
                var t = DateTime.Now - timeStart;
                if (t > TimeOut)
                {
                    //logger.Trace("{0}: cancel async...", Path.GetFileName(fetcher.FileName));
                    client.CancelAsync();
                    fetcher.Cancel();

                    /// NOTE/BUG
                    /// Since I don't know how to abort using CancelAsync().
                    /// I use workaround by dispose client and recreate it.
                    client.Dispose();
                    client = null;
                    Conclude(DownloadedStatusType.TimeOut, string.Format("timeout in {0}!", TimeOut));
                }
            }
        }
        else if (queueList.Count > 0)
        {
            // extract first item
            var q = queueList[0];
            queueList.RemoveAt(0);
            fetcher = new Fetcher(q);

            // downloading
            DownloadFile();
        }

        timer.Change(CheckInterval, Timeout.Infinite);
    }

    private void DownloadFile()
    {
        if (Directory.Exists(fetcher.FileName))
        {
            // error directory exists instead of file, cannot create!
            Conclude(DownloadedStatusType.Error, "is folder!");
            return;
        }

        // prepare download
        if (client == null)
        {
            client = new WebClient();
            client.DownloadFileCompleted += DownloadFileCompleted;
            client.DownloadProgressChanged += DownloadProgressChanged;
        }

        // download file
        try
        {
            timeStart = DateTime.Now;
            client.DownloadFileAsync(new Uri(fetcher.Url), fetcher.FileName);
            //logger.Trace("{0}: downloading...", Path.GetFileName(fetcher.FileName));
        }
        catch (WebException ex)
        {
            //logger.Error(ex);
            Conclude(DownloadedStatusType.Error, ex.Message);
        }
    }

    private void DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
    {
        if (e.Cancelled)
        {
            Conclude(DownloadedStatusType.TimeOut, string.Format("timeout in {0}!", TimeOut));
        }
        else
        {
            if (e.Error != null)
            {
                Conclude(DownloadedStatusType.Error, string.Format("exception: {0}", e.Error.Message));
            }
            else
            {
                Conclude(DownloadedStatusType.Ok, "download ok");
            }
        }
    }

    private void DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
    {
        //if (!fetcher.IsCancelled) {
        // check if we wait too long with same received bytes
        if (e.BytesReceived > fetcher.BytesReceived)
        {
            fetcher.Update(e.TotalBytesToReceive, e.BytesReceived, e.ProgressPercentage);
            fetcher.Progress?.Invoke(fetcher, e.TotalBytesToReceive, e.BytesReceived, e.ProgressPercentage);
            timeStart = DateTime.Now;
        }
        //}
    }

    private void Conclude(DownloadedStatusType status, string log = null)
    {
        //if (!string.IsNullOrWhiteSpace(log))
            //logger.Trace("{0}: {1}", Path.GetFileName(fetcher.FileName), log);
        fetcher.Callback?.Invoke(fetcher, status);
        fetcher = null;
    }
}
