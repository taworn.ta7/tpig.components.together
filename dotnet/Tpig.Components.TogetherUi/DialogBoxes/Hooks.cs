﻿using System;

namespace Tpig.Components.TogetherUi;

public static class Hooks
{
    public static IntPtr HookWindowProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
    {
        // WM_SYSCOMMAND
        if (msg == 0x112)
        {
            var command = wParam.ToInt32() & 0xfff0;
            switch (command)
            {
                // SC_CLOSE
                case 0xf060:
                    handled = true;
                    break;

                // SC_MOVE
                case 0xf010:
                    handled = true;
                    break;

                default:
                    break;
            }
        }
        return IntPtr.Zero;
    }
}
