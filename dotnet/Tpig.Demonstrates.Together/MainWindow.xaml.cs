﻿using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Extensions.Logging;

namespace Tpig.Demonstrates.Together;

/// <summary>
/// Interaction logic for MainWindow.xaml
/// </summary>
public partial class MainWindow : Window
{
    private static ILogger logger = AppBegin.Instance.Logging!.CreateLogger<MainWindow>();

    public MainWindow()
    {
        InitializeComponent();

        Loaded += (sender, e) =>
        {
        };

        Unloaded += (sender, e) =>
        {
        };

        Closed += (sender, e) =>
        {
        };
    }

    // ----------------------------------------------------------------------

}
