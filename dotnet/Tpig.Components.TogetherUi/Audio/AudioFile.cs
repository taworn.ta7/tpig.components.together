﻿using System;
using System.IO;
using NAudio.Wave;

namespace Tpig.Components.TogetherUi;

/// <summary>
/// Play an audio file.
/// </summary>
public class AudioFile : IAudioFile
{
    public AudioFileReader? Reader { get; private set; } = null;

    public WaveOutEvent? Device { get; private set; } = null;

    /// <summary>
    /// Can we play an audio or not?
    /// </summary>
    public bool CanPlay { get; private set; } = false;

    /// <summary>
    /// We are playing an audio or not?
    /// </summary>
    public bool Playing { get; private set; } = false;

    // ----------------------------------------------------------------------

    // Disposing and Construction

    private bool disposedValue = false;

    protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {
                Playing = false;
                if (Device != null)
                {
                    Device.Stop();
                    Device.PlaybackStopped -= PlaybackStopped!;
                    Device.Dispose();
                }
                Reader?.Dispose();
            }
            disposedValue = true;
        }
    }

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    ~AudioFile()
    {
        Dispose(false);
    }

    public AudioFile(string fileName, bool continueEvenError = false)
    {
        try
        {
            FileName = fileName;
            Reader = new AudioFileReader(FileName);
            Device = new WaveOutEvent();
            Device.Init(Reader);
            Device.PlaybackStopped += PlaybackStopped!;
            CanPlay = true;
        }
        catch (Exception)
        {
            if (continueEvenError)
                CanPlay = false;
            else
                throw;
        }
    }

    // ----------------------------------------------------------------------

    /// <summary>
    /// Plays an audio.
    /// </summary>
    public void Play()
    {
        if (CanPlay)
        {
            Reader!.Position = 0;
            Device!.Play();
            Playing = true;
        }
    }

    /// <summary>
    /// Pauses an audio.
    /// </summary>
    public void Pause()
    {
        if (CanPlay)
        {
            Playing = false;
            Device!.Stop();
        }
    }

    /// <summary>
    /// Resumes a paused audio.
    /// </summary>
    public void Resume()
    {
        if (CanPlay)
        {
            Device!.Play();
            Playing = true;
        }
    }

    /// <summary>
    /// Stops an audio.
    /// </summary>
    public void Stop()
    {
        if (CanPlay)
        {
            Playing = false;
            Device!.Stop();
        }
        Reader!.Position = 0;
    }

    // ----------------------------------------------------------------------

    /// <summary>
    /// Volume, scale 0..1.
    /// </summary>
    public float Volume
    {
        get => CanPlay
            ? Device!.Volume
            : 0;
        set
        {
            if (CanPlay)
                Device!.Volume = value;
        }
    }

    /// <summary>
    /// Is playing?
    /// </summary>
    public bool IsPlaying
    {
        get => CanPlay && Device!.PlaybackState == PlaybackState.Playing;
    }

    /// <summary>
    /// Is repeat after playback is end?
    /// </summary>
    public bool IsRepeat
    {
        get;
        set;
    }

    /// <summary>
    /// Current filename of an andio.
    /// </summary>
    public string? FileName
    {
        get;
        private set;
    }

    /// <summary>
    /// Current extension of filename.
    /// </summary>
    public string? Extension
    {
        get => CanPlay
            ? Path.GetExtension(FileName)
            : null;
    }

    /// <summary>
    /// Current file format of an audio.
    /// </summary>
    public AudioType? Format
    {
        get
        {
            if (CanPlay)
            {
                var ext = Extension!.ToLower();
                if (ext == ".wav" || ext == ".wave")
                    return AudioType.Wave;
                if (ext == ".mp3")
                    return AudioType.Mp3;
            }
            return AudioType.Unknown;
        }
    }

    // ----------------------------------------------------------------------

    /// <summary>
    /// Event when the playback is end.
    /// </summary>
    public event EventHandler<StoppedEventArgs>? Stopped;

    private void PlaybackStopped(object sender, StoppedEventArgs e)
    {
        Stopped?.Invoke(this, new StoppedEventArgs() { });
        if (IsRepeat && Playing)
        {
            Reader!.Position = 0;
            Device!.Play();
        }
    }
}
