﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace Tpig.Components.TogetherUi;

public partial class Navigation : ISingleWindow
{
    /// <summary>
    /// Timeout, can be null to disable this feature.
    /// </summary>
    public TimeSpan? DefaultTimeOut { get; set; }

    /// <summary>
    /// Time warning to display, can be null to disable this feature.
    /// </summary>
    public TimeSpan? DefaultTimeWarn { get; set; }

    /// <summary>
    /// Code to display time warning.
    /// </summary>
    public Action<TimeSpan?>? TimeLeft { get; set; }


    /// <summary>
    /// Enabled/disabled timer.
    /// For open dialog box and you should enable/disable timer.
    /// </summary>
    public bool IsEnabled
    {
        get => timer!.IsEnabled;
        set => Enable(value);
    }

    // ----------------------------------------------------------------------

    private void Enable(bool b)
    {
        if (b)
        {
            startTime = DateTime.Now;
            TimeLeft?.Invoke(null);
            timer!.IsEnabled = true;
        }
        else
        {
            timer!.IsEnabled = false;
        }
    }


    private void Tick(object sender, EventArgs e)
    {
        if (Node == null)
            return;
        if (Node.Index <= 0)
            return;

        // check if we have TimeOut in Node, or have DefaultTimeOut
        var timeOut = Node.TimeOut ?? DefaultTimeOut;
        if (timeOut == null)
            return;

        // check if we have TimeWarn in Node, or have DefaultTimeWarn
        var diff = DateTime.Now - startTime;
        var timeUsed = timeOut.Value - diff;
        var timeWarn = Node.TimeWarn ?? DefaultTimeWarn;
        if (timeWarn != null)
        {
            if (timeUsed <= timeWarn)
                TimeLeft?.Invoke(timeUsed);
            else
                TimeLeft?.Invoke(null);
        }

        if (diff > timeOut)
        {
            // run custom code, or default task, which go to first page
            if (Node.Expire != null)
            {
                Node.Expire.Invoke(Node);
            }
            else
            {
                Navigate(Map.List[0]);
            }
        }
    }
}
