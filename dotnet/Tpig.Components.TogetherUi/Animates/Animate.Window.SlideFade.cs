﻿using System;
using System.Windows;
using System.Windows.Media.Animation;

namespace Tpig.Components.TogetherUi;

public static partial class Animate
{
    public static void SlideRightToLeftFadeIn(Window window, Duration timeUsed, EventHandler? handler = null)
    {
        var easing = new SineEase() { EasingMode = EasingMode.EaseInOut };
        var a = new DoubleAnimation()
        {
            From = window.Left + window.Width,
            To = window.Left,
            Duration = timeUsed,
            EasingFunction = easing
        };
        var b = new DoubleAnimation()
        {
            From = 0,
            To = 1,
            Duration = timeUsed,
            EasingFunction = easing
        };
        if (handler != null)
            a.Completed += handler;
        window.BeginAnimation(Window.LeftProperty, a);
        window.BeginAnimation(UIElement.OpacityProperty, b);
    }

    public static void SlideRightToLeftFadeOut(Window window, Duration timeUsed, EventHandler? handler = null)
    {
        var easing = new SineEase() { EasingMode = EasingMode.EaseInOut };
        var a = new DoubleAnimation()
        {
            From = window.Left,
            To = window.Left + window.Width,
            Duration = timeUsed,
            EasingFunction = easing
        };
        var b = new DoubleAnimation()
        {
            From = 1,
            To = 0,
            Duration = timeUsed,
            EasingFunction = easing
        };
        if (handler != null)
            a.Completed += handler;
        window.BeginAnimation(Window.LeftProperty, a);
        window.BeginAnimation(UIElement.OpacityProperty, b);
    }

    // ----------------------------------------------------------------------

    public static void SlideLeftToRightFadeIn(Window window, Duration timeUsed, EventHandler? handler = null)
    {
        var easing = new SineEase() { EasingMode = EasingMode.EaseInOut };
        var a = new DoubleAnimation()
        {
            From = window.Left - window.Width,
            To = window.Left,
            Duration = timeUsed,
            EasingFunction = easing
        };
        var b = new DoubleAnimation()
        {
            From = 0,
            To = 1,
            Duration = timeUsed,
            EasingFunction = easing
        };
        if (handler != null)
            a.Completed += handler;
        window.BeginAnimation(Window.LeftProperty, a);
        window.BeginAnimation(UIElement.OpacityProperty, b);
    }

    public static void SlideLeftToRightFadeOut(Window window, Duration timeUsed, EventHandler? handler = null)
    {
        var easing = new SineEase() { EasingMode = EasingMode.EaseInOut };
        var a = new DoubleAnimation()
        {
            From = window.Left,
            To = window.Left - window.Width,
            Duration = timeUsed,
            EasingFunction = easing
        };
        var b = new DoubleAnimation()
        {
            From = 1,
            To = 0,
            Duration = timeUsed,
            EasingFunction = easing
        };
        if (handler != null)
            a.Completed += handler;
        window.BeginAnimation(Window.LeftProperty, a);
        window.BeginAnimation(UIElement.OpacityProperty, b);
    }

    // ----------------------------------------------------------------------

    public static void SlideBottomToTopFadeIn(Window window, Duration timeUsed, EventHandler? handler = null)
    {
        var easing = new SineEase() { EasingMode = EasingMode.EaseInOut };
        var a = new DoubleAnimation()
        {
            From = window.Top + window.Height,
            To = window.Top,
            Duration = timeUsed,
            EasingFunction = easing
        };
        var b = new DoubleAnimation()
        {
            From = 0,
            To = 1,
            Duration = timeUsed,
            EasingFunction = easing
        };
        if (handler != null)
            a.Completed += handler;
        window.BeginAnimation(Window.TopProperty, a);
        window.BeginAnimation(UIElement.OpacityProperty, b);
    }

    public static void SlideBottomToTopFadeOut(Window window, Duration timeUsed, EventHandler? handler = null)
    {
        var easing = new SineEase() { EasingMode = EasingMode.EaseInOut };
        var a = new DoubleAnimation()
        {
            From = window.Top,
            To = window.Top + window.Height,
            Duration = timeUsed,
            EasingFunction = easing
        };
        var b = new DoubleAnimation()
        {
            From = 1,
            To = 0,
            Duration = timeUsed,
            EasingFunction = easing
        };
        if (handler != null)
            a.Completed += handler;
        window.BeginAnimation(Window.TopProperty, a);
        window.BeginAnimation(UIElement.OpacityProperty, b);
    }

    // ----------------------------------------------------------------------

    public static void SlideTopToBottomFadeIn(Window window, Duration timeUsed, EventHandler? handler = null)
    {
        var easing = new SineEase() { EasingMode = EasingMode.EaseInOut };
        var a = new DoubleAnimation()
        {
            From = window.Top - window.Height,
            To = window.Top,
            Duration = timeUsed,
            EasingFunction = easing
        };
        var b = new DoubleAnimation()
        {
            From = 0,
            To = 1,
            Duration = timeUsed,
            EasingFunction = easing
        };
        if (handler != null)
            a.Completed += handler;
        window.BeginAnimation(Window.TopProperty, a);
        window.BeginAnimation(UIElement.OpacityProperty, b);
    }

    public static void SlideTopToBottomFadeOut(Window window, Duration timeUsed, EventHandler? handler = null)
    {
        var easing = new SineEase() { EasingMode = EasingMode.EaseInOut };
        var a = new DoubleAnimation()
        {
            From = window.Top,
            To = window.Top - window.Height,
            Duration = timeUsed,
            EasingFunction = easing
        };
        var b = new DoubleAnimation()
        {
            From = 1,
            To = 0,
            Duration = timeUsed,
            EasingFunction = easing
        };
        if (handler != null)
            a.Completed += handler;
        window.BeginAnimation(Window.TopProperty, a);
        window.BeginAnimation(UIElement.OpacityProperty, b);
    }
}
