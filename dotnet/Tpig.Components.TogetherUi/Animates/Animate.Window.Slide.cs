﻿using System;
using System.Windows;
using System.Windows.Media.Animation;

namespace Tpig.Components.TogetherUi;

public static partial class Animate
{
    public static void SlideRightToLeftIn(Window window, Duration timeUsed, EventHandler? handler = null)
    {
        var easing = new SineEase() { EasingMode = EasingMode.EaseInOut };
        var a = new DoubleAnimation()
        {
            From = window.Left + window.Width,
            To = window.Left,
            Duration = timeUsed,
            EasingFunction = easing
        };
        if (handler != null)
            a.Completed += handler;
        window.BeginAnimation(Window.LeftProperty, a);
    }

    public static void SlideRightToLeftOut(Window window, Duration timeUsed, EventHandler? handler = null)
    {
        var easing = new SineEase() { EasingMode = EasingMode.EaseInOut };
        var a = new DoubleAnimation()
        {
            From = window.Left,
            To = window.Left + window.Width,
            Duration = timeUsed,
            EasingFunction = easing
        };
        if (handler != null)
            a.Completed += handler;
        window.BeginAnimation(Window.LeftProperty, a);
    }

    // ----------------------------------------------------------------------

    public static void SlideLeftToRightIn(Window window, Duration timeUsed, EventHandler? handler = null)
    {
        var easing = new SineEase() { EasingMode = EasingMode.EaseInOut };
        var a = new DoubleAnimation()
        {
            From = window.Left - window.Width,
            To = window.Left,
            Duration = timeUsed,
            EasingFunction = easing
        };
        if (handler != null)
            a.Completed += handler;
        window.BeginAnimation(Window.LeftProperty, a);
    }

    public static void SlideLeftToRightOut(Window window, Duration timeUsed, EventHandler? handler = null)
    {
        var easing = new SineEase() { EasingMode = EasingMode.EaseInOut };
        var a = new DoubleAnimation()
        {
            From = window.Left,
            To = window.Left - window.Width,
            Duration = timeUsed,
            EasingFunction = easing
        };
        if (handler != null)
            a.Completed += handler;
        window.BeginAnimation(Window.LeftProperty, a);
    }

    // ----------------------------------------------------------------------

    public static void SlideBottomToTopIn(Window window, Duration timeUsed, EventHandler? handler = null)
    {
        var easing = new SineEase() { EasingMode = EasingMode.EaseInOut };
        var a = new DoubleAnimation()
        {
            From = window.Top + window.Height,
            To = window.Top,
            Duration = timeUsed,
            EasingFunction = easing
        };
        if (handler != null)
            a.Completed += handler;
        window.BeginAnimation(Window.TopProperty, a);
    }

    public static void SlideBottomToTopOut(Window window, Duration timeUsed, EventHandler? handler = null)
    {
        var easing = new SineEase() { EasingMode = EasingMode.EaseInOut };
        var a = new DoubleAnimation()
        {
            From = window.Top,
            To = window.Top + window.Height,
            Duration = timeUsed,
            EasingFunction = easing
        };
        if (handler != null)
            a.Completed += handler;
        window.BeginAnimation(Window.TopProperty, a);
    }

    // ----------------------------------------------------------------------

    public static void SlideTopToBottomIn(Window window, Duration timeUsed, EventHandler? handler = null)
    {
        var easing = new SineEase() { EasingMode = EasingMode.EaseInOut };
        var a = new DoubleAnimation()
        {
            From = window.Top - window.Height,
            To = window.Top,
            Duration = timeUsed,
            EasingFunction = easing
        };
        if (handler != null)
            a.Completed += handler;
        window.BeginAnimation(Window.TopProperty, a);
    }

    public static void SlideTopToBottomOut(Window window, Duration timeUsed, EventHandler? handler = null)
    {
        var easing = new SineEase() { EasingMode = EasingMode.EaseInOut };
        var a = new DoubleAnimation()
        {
            From = window.Top,
            To = window.Top - window.Height,
            Duration = timeUsed,
            EasingFunction = easing
        };
        if (handler != null)
            a.Completed += handler;
        window.BeginAnimation(Window.TopProperty, a);
    }
}
