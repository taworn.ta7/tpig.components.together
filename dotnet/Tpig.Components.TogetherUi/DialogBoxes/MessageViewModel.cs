﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using System.Windows.Media;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;

namespace Tpig.Components.TogetherUi;

public partial class MessageViewModel : ObservableObject, INotifyPropertyChanged
{
    // Caption

    [ObservableProperty]
    private string caption = string.Empty;

    public bool ShowCaption { get => Caption != string.Empty; }

    // ----------------------------------------------------------------------

    // Message

    [ObservableProperty]
    private string message = string.Empty;

    // ----------------------------------------------------------------------

    // Background Color

    [ObservableProperty]
    private Color? backgroundColor = null;

    // ----------------------------------------------------------------------

    // Button Groups

    public enum Group
    {
        Close = 0,
        OkCancel,
        YesNo,
        RetryCancel,
    }

    [ObservableProperty]
    private Group buttons = Group.Close;

    public bool GroupClose { get => Buttons == Group.Close; }
    public bool GroupOkCancel { get => Buttons == Group.OkCancel; }
    public bool GroupYesNo { get => Buttons == Group.YesNo; }
    public bool GroupRetryCancel { get => Buttons == Group.RetryCancel; }

    // ----------------------------------------------------------------------

    // Adorn Image

    public enum Adorn
    {
        None = 0,
        Information,
        Question,
        Warning,
        Error,
    }

    [ObservableProperty]
    private Adorn decorate = Adorn.None;

    public bool AdornInformation { get => Decorate == Adorn.Information; }
    public bool AdornQuestion { get => Decorate == Adorn.Question; }
    public bool AdornWarning { get => Decorate == Adorn.Warning; }
    public bool AdornError { get => Decorate == Adorn.Error; }

    // ----------------------------------------------------------------------

    // Auto Closing Timer

    public class AutoCloseTimer
    {
        // resource identifier loaded by LoadText()
        // it must be one {0} to format seconds
        public string Formatter { get; set; } = string.Empty;

        // timeout
        public TimeSpan TimeOut { get; set; }

        // result in case timeout
        public Id Result { get; set; } = Id.Close;
    }

    [ObservableProperty]
    private AutoCloseTimer? autoClose = null;

    // ----------------------------------------------------------------------

    // Result

    public enum Id
    {
        Close = 0,
        Ok,
        Cancel,
        Yes,
        No,
        Retry,
    }

    public static Id IdClose { get => Id.Close; }
    public static Id IdOk { get => Id.Ok; }
    public static Id IdCancel { get => Id.Cancel; }
    public static Id IdYes { get => Id.Yes; }
    public static Id IdNo { get => Id.No; }
    public static Id IdRetry { get => Id.Retry; }

    [ObservableProperty]
    private Id result = Id.Close;

    // ----------------------------------------------------------------------

    // Action When Dialog Closed

    public Action<Id>? ClosedDelegate { get; set; }

    [RelayCommand]
    private void Closed(object o)
    {
        Result = (Id)o;
        ClosedDelegate?.Invoke(Result);
    }

    // ----------------------------------------------------------------------

    // Constructor

    public MessageViewModel()
    {
    }

    // ----------------------------------------------------------------------

    // Constructor with Packaged Dialog Set

    public enum DialogSet
    {
        NormalSet = 0,
        OkSet,
        OkCancelSet,
        YesNoSet,
        RetrySet,
        PassSet,
        InformationSet,
        QuestionSet,
        WarningSet,
        ErrorSet,
    }

    public MessageViewModel(DialogSet set)
    {
        BackgroundColor = null;
        Buttons = Group.Close;
        Decorate = Adorn.None;
        switch (set)
        {
            default:
            case DialogSet.NormalSet:
                break;

            case DialogSet.OkCancelSet:
                Buttons = Group.OkCancel;
                break;
            case DialogSet.YesNoSet:
                Buttons = Group.YesNo;
                break;
            case DialogSet.RetrySet:
                Buttons = Group.RetryCancel;
                Decorate = Adorn.Question;
                break;

            case DialogSet.PassSet:
                BackgroundColor = Color.FromArgb(0x80, 0, 0x80, 0);
                break;

            case DialogSet.InformationSet:
                Buttons = Group.Close;
                Decorate = Adorn.Information;
                BackgroundColor = Color.FromArgb(0x80, 0, 0x80, 0x40);
                break;
            case DialogSet.QuestionSet:
                Buttons = Group.YesNo;
                Decorate = Adorn.Question;
                BackgroundColor = Color.FromArgb(0x80, 0, 0x40, 0x80);
                break;
            case DialogSet.WarningSet:
                Buttons = Group.Close;
                Decorate = Adorn.Warning;
                BackgroundColor = Color.FromArgb(0x80, 0x80, 0x80, 0);
                break;
            case DialogSet.ErrorSet:
                Buttons = Group.Close;
                Decorate = Adorn.Error;
                BackgroundColor = Color.FromArgb(0x80, 0x80, 0, 0);
                break;
        }
    }
}
