﻿using System;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Microsoft.Xaml.Behaviors;

namespace Tpig.Components.TogetherUi;

/// <summary>
/// Please note that regular expression is not defense malform.
/// Beware it input from this textbox.
/// </summary>
public class RegularExpressionBehavior : Behavior<TextBox>
{
    public static readonly DependencyProperty ExpressionProperty =
        DependencyProperty.Register("Expression", typeof(string), typeof(RegularExpressionBehavior), new FrameworkPropertyMetadata(".*"));

    public string Expression
    {
        get => (string)GetValue(ExpressionProperty);
        set { SetValue(ExpressionProperty, value); }
    }

    private Regex regex = null!;

    protected override void OnAttached()
    {
        base.OnAttached();
        regex = new Regex(Expression);
        AssociatedObject.PreviewKeyDown += PreviewKeyDown;
        AssociatedObject.PreviewTextInput += PreviewTextInput;
        DataObject.AddPastingHandler(AssociatedObject, PastingHandler);
    }

    protected override void OnDetaching()
    {
        DataObject.RemovePastingHandler(AssociatedObject, PastingHandler);
        AssociatedObject.PreviewTextInput -= PreviewTextInput;
        AssociatedObject.PreviewKeyDown -= PreviewKeyDown;
        regex = null!;
        base.OnDetaching();
    }

    private string TryCombineText(string inserted)
    {
        var text = AssociatedObject.Text;
        if (AssociatedObject.SelectionLength > 0)
            text = text.Remove(AssociatedObject.CaretIndex, AssociatedObject.SelectionLength);
        if (inserted != null)
            text = text.Insert(AssociatedObject.CaretIndex, inserted);
        return text;
    }

    private void PreviewKeyDown(object sender, KeyEventArgs e)
    {
        if (e.Key == Key.Space)
        {
            var combinedText = TryCombineText(" ");
            e.Handled = !regex.IsMatch(combinedText);
            if (AssociatedObject.MaxHeight > 0 && combinedText.Length > AssociatedObject.MaxHeight)
            {
                e.Handled = true;
            }
        }
    }

    private void PreviewTextInput(object sender, TextCompositionEventArgs e)
    {
        var combinedText = TryCombineText(e.Text);
        e.Handled = !regex.IsMatch(combinedText);
        if (AssociatedObject.MaxHeight > 0 && combinedText.Length > AssociatedObject.MaxHeight)
        {
            e.Handled = true;
        }
    }

    private void PastingHandler(object sender, DataObjectPastingEventArgs e)
    {
        if (e.DataObject.GetDataPresent(DataFormats.Text))
        {
            var text = Convert.ToString(e.DataObject.GetData(DataFormats.Text))!;
            var combinedText = TryCombineText(text);
            var ok = regex.IsMatch(combinedText);
            if (!ok)
                e.CancelCommand();
        }
        else
            e.CancelCommand();
    }
}
