﻿using NAudio.Wave;

namespace Tpig.Components.TogetherUi;

/// <summary>
/// Audio file type.
/// </summary>
public enum AudioType
{
    Unknown = -1,
    Wave = 0,
    Mp3 = 1,
}


/// <summary>
/// Play an audio file.
/// </summary>
public interface IAudioFile : IDisposable
{
    /// <summary>
    /// Can we play an audio or not?
    /// </summary>
    bool CanPlay { get; }

    /// <summary>
    /// Plays an audio.
    /// </summary>
    void Play();

    /// <summary>
    /// Pauses an audio.
    /// </summary>
    void Pause();

    /// <summary>
    /// Resumes a paused audio.
    /// </summary>
    void Resume();

    /// <summary>
    /// Stops an audio.
    /// </summary>
    void Stop();

    /// <summary>
    /// Volume, scale 0..1.
    /// </summary>
    float Volume { get; set; }

    /// <summary>
    /// Is playing?
    /// </summary>
    bool IsPlaying { get; }

    /// <summary>
    /// Is repeat after playback is end?
    /// </summary>
    bool IsRepeat { get; set; }

    /// <summary>
    /// Current filename of an andio.
    /// </summary>
    string? FileName { get; }

    /// <summary>
    /// Current extension of filename.
    /// </summary>
    string? Extension { get; }

    /// <summary>
    /// Current file format of an audio, may be AudioType == Unknown but it still work.
    /// </summary>
    AudioType? Format { get; }

    /// <summary>
    /// Event when the playback is end.
    /// </summary>
    event EventHandler<StoppedEventArgs> Stopped;
}
