# Tpig.Components.TogetherUi

It's my work on C# .NET WPF and make libraries to reduce my time.  Later, I decide to merge these libraries:

* [Audio](https://gitlab.com/taworn.ta7/tpig.legacy/-/tree/main/tpig.components.audios)
* [CustomUi](https://gitlab.com/taworn.ta7/tpig.legacy/-/tree/main/tpig.components.customui)
* [Navigators, change name to Single Window](https://gitlab.com/taworn.ta7/tpig.legacy/-/tree/main/tpig.components.navigators)
* [Soft Keyboards](https://gitlab.com/taworn.ta7/tpig.legacy/-/tree/main/tpig.components.softkeyboards)
* [Generic Helpers](https://gitlab.com/taworn.ta7/tpig.legacy/-/tree/main/tpig.helpers)
* [Expanded Dialog Boxes and SceneTimer, which used with Single Window](https://gitlab.com/taworn.ta7/tpig.legacy/-/tree/main/tpig.together)

also, I changed code to fix null warning.

Due to [NuGet](https://www.nuget.org) has limited README feature.  You should read in [my repository](https://gitlab.com/taworn.ta7/tpig.components.together).




# Features

## Animation

Please see demo in various code, such as: Tpig.Demonstrates.SingleWindow, to see how Animates work.

Please see demo about SequenceAnimation and SequenceAnimationRenderer in Tpig.Demonstrates.SequenceAnim.


## Behaviors

Please see demo about behaviors in Tpig.Demonstrates.Behaviors.


## Audio

Please see [Audio](./README-Audio.md).


## Dialog Boxes

Please see [Dialog Boxes](./README-DialogBoxes.md).


## Single Window

Please see [Single Window](./README-SingleWindow.md).


## Soft Keyboards

Please see [Soft Keyboards](./README-SoftKeyboards.md).




# Release Notes

Please see [CHANGELOG](../Tpig.Components.TogetherUi/CHANGELOG.md).




# Thank For These People

* [Crystal Clear icons](https://commons.wikimedia.org/wiki/Crystal_Clear)

* [Nuvola icons](https://commons.wikimedia.org/wiki/Category:Nuvola_icons)

* Loading image, don't know where I get it, sorry T_T

* [RelayCommand](https://stackoverflow.com/questions/22285866/why-relaycommand), removed, but still have good article

* [FindVisualChildren](https://stackoverflow.com/questions/974598/find-all-controls-in-wpf-window-by-type)

* GetDescendantByType, I can't remember this function, sorry T_T

* [InputSimulatorStandard for Soft Keyboards](https://github.com/GregsStack/InputSimulatorStandard)

* [NAudio for Audio](https://github.com/naudio/NAudio).

* [Localization for Message Box, Me ^_^](https://gitlab.com/taworn.ta7/tpig.components.localization)




# Last

Sorry, but I'm not good at English. T_T

