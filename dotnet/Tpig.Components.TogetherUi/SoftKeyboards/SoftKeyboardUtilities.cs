﻿using System.Windows;

namespace Tpig.Components.TogetherUi;

public static class SoftKeyboardUtilities
{
    public static SoftKeyboardControl? FindSoftKeyboard(FrameworkElement control)
    {
        var window = Window.GetWindow(control);
        return Helpful.GetDescendantByType<SoftKeyboardControl>(window);
    }
}
