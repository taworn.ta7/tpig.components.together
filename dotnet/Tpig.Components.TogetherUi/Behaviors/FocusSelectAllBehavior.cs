﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using Microsoft.Xaml.Behaviors;

namespace Tpig.Components.TogetherUi;

public static class FocusSelectAllBehavior
{
    public static bool GetIsFocusSelectAll(DependencyObject d)
    {
        return (bool)d.GetValue(IsFocusSelectAllProperty);
    }

    public static void SetIsFocusSelectAll(DependencyObject d, bool value)
    {
        d.SetValue(IsFocusSelectAllProperty, value);
    }

    public static readonly DependencyProperty IsFocusSelectAllProperty = DependencyProperty.RegisterAttached("IsFocusSelectAll", typeof(bool), typeof(FocusSelectAllBehavior), new PropertyMetadata(false, OnIsFocusSelectAllChanged));

    private static void OnIsFocusSelectAllChanged(object sender, DependencyPropertyChangedEventArgs e)
    {
        var o = sender as Control;
        if (o != null)
        {
            var isFocusSelectAll = (bool)e.NewValue;
            if (isFocusSelectAll)
            {
                o.GotFocus += GotFocus;
            }
            else
            {
                o.GotFocus -= GotFocus;
            }
        }
    }

    private static void GotFocus(object sender, RoutedEventArgs e)
    {
        if (sender is TextBoxBase)
        {
            var o = sender as TextBoxBase;
            o?.SelectAll();
        }
        else if (sender is PasswordBox)
        {
            var o = sender as PasswordBox;
            o?.SelectAll();
        }
    }
}


public class BlendFocusSelectAllBehavior : Behavior<TextBoxBase>
{
    protected override void OnAttached()
    {
        base.OnAttached();
        AssociatedObject.GotFocus += GotFocus;
    }

    protected override void OnDetaching()
    {
        AssociatedObject.GotFocus -= GotFocus;
        base.OnDetaching();
    }

    private static void GotFocus(object sender, RoutedEventArgs e)
    {
        if (sender is TextBoxBase)
        {
            var o = sender as TextBoxBase;
            o?.SelectAll();
        }
        else if (sender is PasswordBox)
        {
            var o = sender as PasswordBox;
            o?.SelectAll();
        }
    }
}
