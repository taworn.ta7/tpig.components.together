﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using Microsoft.Xaml.Behaviors;

namespace Tpig.Components.TogetherUi;

public static class DigitsBehavior
{
    public static bool GetIsDigits(DependencyObject d)
    {
        return (bool)d.GetValue(IsDigitsProperty);
    }

    public static void SetIsDigits(DependencyObject d, bool value)
    {
        d.SetValue(IsDigitsProperty, value);
    }

    public static readonly DependencyProperty IsDigitsProperty = DependencyProperty.RegisterAttached("IsDigits", typeof(bool), typeof(DigitsBehavior), new PropertyMetadata(false, OnIsDigitsChanged));

    private static void OnIsDigitsChanged(object sender, DependencyPropertyChangedEventArgs e)
    {
        if (sender is TextBox o)
        {
            var isDigitOnly = (bool)e.NewValue;
            if (isDigitOnly)
            {
                o.PreviewKeyDown += PreviewKeyDown;
                o.PreviewTextInput += PreviewTextInput;
                DataObject.AddPastingHandler(o, PastingHandler);
            }
            else
            {
                DataObject.RemovePastingHandler(o, PastingHandler);
                o.PreviewTextInput -= PreviewTextInput;
                o.PreviewKeyDown -= PreviewKeyDown;
            }
        }
    }

    private static void PreviewKeyDown(object sender, KeyEventArgs e)
    {
        if (e.Key == Key.Space)
            e.Handled = true;
    }

    private static void PreviewTextInput(object sender, TextCompositionEventArgs e)
    {
        e.Handled = e.Text.Any(ch => !Char.IsDigit(ch));
    }

    private static void PastingHandler(object sender, DataObjectPastingEventArgs e)
    {
        if (e.DataObject.GetDataPresent(DataFormats.Text))
        {
            var text = Convert.ToString(e.DataObject.GetData(DataFormats.Text))!;
            var ok = text.All(ch => Char.IsDigit(ch));
            if (!ok)
                e.CancelCommand();
        }
        else
            e.CancelCommand();
    }
}


public class BlendDigitsBehavior : Behavior<TextBoxBase>
{
    protected override void OnAttached()
    {
        base.OnAttached();
        AssociatedObject.PreviewKeyDown += PreviewKeyDown;
        AssociatedObject.PreviewTextInput += PreviewTextInput;
        DataObject.AddPastingHandler(AssociatedObject, PastingHandler);
    }

    protected override void OnDetaching()
    {
        DataObject.RemovePastingHandler(AssociatedObject, PastingHandler);
        AssociatedObject.PreviewTextInput -= PreviewTextInput;
        AssociatedObject.PreviewKeyDown -= PreviewKeyDown;
        base.OnDetaching();
    }

    private static void PreviewKeyDown(object sender, KeyEventArgs e)
    {
        if (e.Key == Key.Space)
            e.Handled = true;
    }

    private static void PreviewTextInput(object sender, TextCompositionEventArgs e)
    {
        e.Handled = e.Text.Any(ch => !Char.IsDigit(ch));
    }

    private static void PastingHandler(object sender, DataObjectPastingEventArgs e)
    {
        if (e.DataObject.GetDataPresent(DataFormats.Text))
        {
            var text = Convert.ToString(e.DataObject.GetData(DataFormats.Text))!;
            var ok = text.All(ch => Char.IsDigit(ch));
            if (!ok)
                e.CancelCommand();
        }
        else
            e.CancelCommand();
    }
}


public class BlendDigitsPasswordBehavior : Behavior<PasswordBox>
{
    protected override void OnAttached()
    {
        base.OnAttached();
        AssociatedObject.PreviewKeyDown += PreviewKeyDown;
        AssociatedObject.PreviewTextInput += PreviewTextInput;
        DataObject.AddPastingHandler(AssociatedObject, PastingHandler);
    }

    protected override void OnDetaching()
    {
        DataObject.RemovePastingHandler(AssociatedObject, PastingHandler);
        AssociatedObject.PreviewTextInput -= PreviewTextInput;
        AssociatedObject.PreviewKeyDown -= PreviewKeyDown;
        base.OnDetaching();
    }

    private static void PreviewKeyDown(object sender, KeyEventArgs e)
    {
        if (e.Key == Key.Space)
            e.Handled = true;
    }

    private static void PreviewTextInput(object sender, TextCompositionEventArgs e)
    {
        e.Handled = e.Text.Any(ch => !Char.IsDigit(ch));
    }

    private static void PastingHandler(object sender, DataObjectPastingEventArgs e)
    {
        if (e.DataObject.GetDataPresent(DataFormats.Text))
        {
            var text = Convert.ToString(e.DataObject.GetData(DataFormats.Text))!;
            var ok = text.All(ch => Char.IsDigit(ch));
            if (!ok)
                e.CancelCommand();
        }
        else
            e.CancelCommand();
    }
}
