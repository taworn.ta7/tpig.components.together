## Dialog Boxes

The dialog boxes have 6 classes to disable current UI, wait for something, and enable again.  They are:

* AlphaWaitControl and AlphaWaitWindow
* ExpandedWaitControl and ExpandedWaitWindow
* WaitControl and WaitWindow

It also provide classes to display message box.  They are:

* MessageWindow
* ExpandedMessageWindow

Please see demo about dialog box(es) in Tpig.Demonstrates.DialogBox and Tpig.Demonstrates.ExpandedDialogBox.

![Screenshot 1](../Screenshots/DialogBox-0.png) | ![Screenshot 2](../Screenshots/DialogBox-1.png)
![Screenshot 3](../Screenshots/DialogBox-2.png) | ![Screenshot 4](../Screenshots/ExpandedDialogBox-0.png)
![Screenshot 5](../Screenshots/ExpandedDialogBox-1.png) | ![Screenshot 6](../Screenshots/ExpandedDialogBox-2.png)
