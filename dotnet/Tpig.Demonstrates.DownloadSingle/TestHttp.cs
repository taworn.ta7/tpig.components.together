﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tpig.Components.Together;

namespace Tpig.Demonstrates.DownloadSingle;

public static class TestHttp
{
    public static void BeforeDownload(HttpClient client)
    {
        Console.WriteLine("BeforeDownload:");
    }

    public static void ProgressChanged(long totalDownloadedBytes, double? percentage)
    {
        Console.WriteLine("ProgressChanged: {0}, {1}%", totalDownloadedBytes, percentage);
    }

    public static void AfterDownloadedelegate(bool success)
    {
        Console.WriteLine("AfterDownloaded: {0}", success);
    }

    public static async Task DownloadAsync()
    {
        var downloadFrom = "http://localhost:8080/88e71c01-1a5f-4e28-a873-ba554906861e.tmp";
        var downloadTo = Path.Combine(Directory.GetCurrentDirectory(), "abc.tmp");
        Console.WriteLine("Download From: {0}", downloadFrom);
        Console.WriteLine("Download To:   {0}", downloadTo);

        var client = new HttpClientEx(downloadFrom, downloadTo);
        client.BeforeDownload += TestHttp.BeforeDownload;
        client.ProgressChanged += TestHttp.ProgressChanged;
        client.AfterDownloaded += TestHttp.AfterDownloadedelegate;

        await client.DownloadAsync();
    }
}
