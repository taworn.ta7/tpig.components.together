## Audio

A simple audio, backend NAudio, playing.

Create:

    var audio = new AudioFile(dialog.FileName);

    -- or --

    var audio = new AudioFile(dialog.FileName, true);  // for tolerate file not found error

Play, Pause, Resume and Stop:

    audio.Play();
    audio.Pause();
    audio.Resume();
    audio.Stop();

Set volume, 0 to 1:

    audio.Volume = 0.5;

Repeat, or just stop when audio file end:

    audio.IsRepeat = true/false;

Destroy (optional):

    audio.Dispose();

Please see demo about play audio in Tpig.Demonstrates.Audio.

![Screenshot 1](../Screenshots/Audio-0.png)
