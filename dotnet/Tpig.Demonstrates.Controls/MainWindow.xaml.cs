﻿using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Tpig.Components.Localization;

namespace Tpig.Demonstrates.Controls;

/// <summary>
/// Interaction logic for MainWindow.xaml
/// </summary>
public partial class MainWindow : Window
{
    private MainViewModel? viewModel;

    public MainWindow()
    {
        InitializeComponent();
        LocaleManager.Instance.Current = "en";
        Localize.Load(this);

        Loaded += (sender, e) =>
        {
            viewModel = new MainViewModel();
            viewModel.ChangeLocaleDelegate += ChangeLocaleDelegate;
            DataContext = viewModel;

            var locale = LocaleManager.Instance.Current;
            ChangeLocaleEnButton.IsChecked = ChangeLocaleEnButton.CommandParameter.ToString()?.Equals(locale);
            ChangeLocaleThButton.IsChecked = ChangeLocaleThButton.CommandParameter.ToString()?.Equals(locale);
        };

        Unloaded += (sender, e) =>
        {
            DataContext = null;
        };
    }

    // ----------------------------------------------------------------------

    private void ChangeLocaleDelegate(object o)
    {
        var locale = o as string;
        LocaleManager.Instance.Current = locale!;
    }

    private void Click0(object sender, RoutedEventArgs e)
    {
        MessageBox.Show(this, "First button clicked!", "Hello ^_^", MessageBoxButton.OK, MessageBoxImage.Information);
    }

    private void Click1(object sender, RoutedEventArgs e)
    {
        MessageBox.Show(this, "Second button must NOT be clicked!", "Not good!", MessageBoxButton.OK, MessageBoxImage.Error);
    }
}
