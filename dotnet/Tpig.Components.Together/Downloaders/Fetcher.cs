﻿namespace Tpig.Components.Together;

internal class Fetcher : Queue
{
    public long TotalBytesToReceive { get; private set; } = 0;
    public long BytesReceived { get; private set; } = 0;
    public int ProgressPercentage { get; private set; } = 0;

    public bool IsCancelled { get; private set; } = false;

    public Fetcher(Queue q)
    {
        Url = q.Url;
        FileName = q.FileName;
        Callback = q.Callback;
        Progress = q.Progress;
        UserData = q.UserData;
    }

    public void Update(long totalBytesToReceive, long bytesReceived, int progressPercentage)
    {
        TotalBytesToReceive = totalBytesToReceive;
        BytesReceived = bytesReceived;
        ProgressPercentage = progressPercentage;
    }

    public void Cancel()
    {
        IsCancelled = true;
    }
}
