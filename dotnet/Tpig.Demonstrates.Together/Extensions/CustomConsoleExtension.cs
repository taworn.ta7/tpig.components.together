﻿using System;
using System.IO;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.Extensions.Logging.Console;
using Microsoft.Extensions.Options;

namespace Tpig.Demonstrates.Together.Extensions;

public static class CustomConsoleExtension
{
    public static ILoggingBuilder AddCustomConsoleFormatter(
      this ILoggingBuilder builder,
      Action<CustomFormatterOptions> configure) => builder
        .AddConsole(options => options.FormatterName = "custom")
        .AddConsoleFormatter<CustomFormatter, CustomFormatterOptions>(configure);
}


public sealed class CustomFormatterOptions : ConsoleFormatterOptions
{
}


public sealed class CustomFormatter : ConsoleFormatter, IDisposable
{
    private readonly IDisposable? disposable;
    private CustomFormatterOptions options;

    public CustomFormatter(IOptionsMonitor<CustomFormatterOptions> o)
        : base("custom") => (disposable, options) = (o.OnChange(ReloadLoggerOptions), o.CurrentValue);

    public void Dispose() => disposable?.Dispose();

    private void ReloadLoggerOptions(CustomFormatterOptions o) => options = o;

    public override void Write<TState>(
        in LogEntry<TState> entry,
        IExternalScopeProvider? provider,
        TextWriter writer)
    {
        string? message = entry.Formatter?.Invoke(entry.State, entry.Exception);
        if (message is null)
            return;

        string format = options.TimestampFormat ?? "yyyy-MM-dd HH:mm:ss.ffff";
        string now = DateTime.Now.ToString(format);

        string level = entry.LogLevel.ToString().Substring(0, 3).ToUpper();
        string color;
        if (entry.LogLevel == LogLevel.Trace)
            color = "\x1b[94m";
        else if (entry.LogLevel == LogLevel.Debug)
            color = "\x1b[92m";
        else if (entry.LogLevel == LogLevel.Information)
            color = "\x1b[93m";
        else if (entry.LogLevel == LogLevel.Warning)
            color = "\x1b[33m";
        else if (entry.LogLevel == LogLevel.Error)
            color = "\x1b[91m";
        else if (entry.LogLevel == LogLevel.Critical)
            color = "\x1b[97;41m";
        else
            color = "\x1b[0m";

        writer.Write("{0}{1} [{2}]\u001b[0m", color, now, level);

        string[] categories = entry.Category.Split('.');
        string category;
        if (categories.Length > 0)
            category = categories[categories.Length - 1];
        else
            category = entry.Category;

        writer.Write(" \u001b[45m{0}", category);
        if (options.IncludeScopes && provider != null)
        {
            provider.ForEachScope((scope, state) =>
            {
                state.Write(" => ");
                state.Write(scope);
            }, writer);
        }
        writer.Write("\u001b[0m | ");

        writer.WriteLine(message);
    }
}
