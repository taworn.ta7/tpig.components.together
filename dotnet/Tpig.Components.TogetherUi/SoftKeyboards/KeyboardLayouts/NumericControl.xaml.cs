﻿using System.Windows.Controls;
using System.Windows.Input;
using InputSimulatorStandard.Native;

namespace Tpig.Components.TogetherUi.KeyboardLayouts;

/// <summary>
/// Interaction logic for NumericControl.xaml
/// </summary>
public partial class NumericControl : UserControl
{
    private KeyboardViewModel? viewModel = null;

    public NumericControl(KeyboardViewModel vm)
    {
        InitializeComponent();

        Loaded += (sender, e) =>
        {
            viewModel = vm;
            viewModel.NormalKeyDelegate += NormalKeyDelegate;
            viewModel.SpecialKeyDelegate += SpecialKeyDelegate;
            DataContext = viewModel;
        };
        Unloaded += (sender, e) =>
        {
            DataContext = null;
            //viewModel.SpecialKeyDelegate -= SpecialKeyDelegate;
            //viewModel.NormalKeyDelegate -= NormalKeyDelegate;
            viewModel = null;
        };
    }

    private void NormalKeyDelegate(object o)
    {
        var s = o as string;
        //Console.WriteLine("soft key: {0}", s);
        viewModel?.Input?.Keyboard.TextEntry(s);
    }

    private void SpecialKeyDelegate(object o)
    {
        var s = o as string;
        //Console.WriteLine("special key: {0}", s);
        if (s == "*")
        {
            var i = Keyboard.FocusedElement;
            if (i is TextBox)
            {
                var t = i as TextBox;
                t!.Text = "";
            }
            else if (i is PasswordBox)
            {
                var t = i as PasswordBox;
                t!.Password = "";
            }
        }
        else if (s == "1")
        {
            viewModel?.Input?.Keyboard.KeyPress(VirtualKeyCode.BACK);
        }
        else if (s == "Hide")
        {
            var kb = SoftKeyboardUtilities.FindSoftKeyboard(this);
            kb?.Hide();
        }
        else if (s == "Enter")
        {
            var control = Keyboard.FocusedElement as Control;
            control?.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
        }
    }
}
