﻿using System.Configuration;
using System.Data;
using System.Windows;

namespace Tpig.Demonstrates.Together;

/// <summary>
/// Interaction logic for App.xaml
/// </summary>
public partial class App : Application
{
    public App()
    {
        AppBegin.Instance.Print();
        AppConfigs.Instance.Print();
        SharedModule.Instance.Print();
    }
}
