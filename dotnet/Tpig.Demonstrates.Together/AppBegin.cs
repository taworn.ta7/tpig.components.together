﻿using System;
using System.IO;
using System.Reflection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;
using Tpig.Demonstrates.Together.Extensions;

namespace Tpig.Demonstrates.Together;

/// <summary>
/// Beginning app to initialize components.
/// </summary>
public class AppBegin
{
    public const string AppName = "TogetherApp";

    //
    // Folders
    //

    public Boolean Portable { get; private set; } = false;
    public string AppPath { get; private set; } = string.Empty;
    public string DataPath { get; private set; } = string.Empty;
    public string LogPath { get; private set; } = string.Empty;
    public string TempPath { get; private set; } = string.Empty;

    //
    // Configuration and Logging
    //

    public IConfiguration Config { get; private set; } = null!;
    public ILoggerFactory Logging { get; private set; } = null!;

    private static ILogger logger = null!;

    public static AppBegin Instance
    {
        get => instance ??= new AppBegin();
    }
    private static AppBegin instance = null!;

    private AppBegin()
    {
        // retrieves App path
        AppPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)!;

        // checks if we run in portable mode
        // * App folder must be name '.../App'
        // * Data folder must be lay side with App path and name '.../Data', and it must exists.
        var fileName = Path.GetFileName(AppPath);
        if (fileName.ToLower().Equals("app"))
        {
            // app path is like '.../App', checks data path
            var parent = Path.GetDirectoryName(AppPath);
            var data = Path.Combine(parent!, "Data");
            if (Directory.Exists(data))
            {
                // this will result 2 folders
                // * App/*.exe,dll - app folder
                // * Data/         - data folder, separate folder
                Portable = true;
                DataPath = data;
            }
        }

        // not portable, checks with another 'portable' file, just blank-zero file, must be same as App path
        if (!Portable)
        {
            var port = Path.Combine(AppPath, "portable");
            if (File.Exists(port))
            {
                // if you use 'portable' file, you will get this
                // .../App/*.exe,dll - app folder
                // .../App/Data/     - data folder, inside app folder
                Portable = true;
                DataPath = Path.Combine(AppPath, "Data");
            }
        }

        // others paths
        if (Portable)
        {
            LogPath = Path.Combine(DataPath, "logs");
            TempPath = Path.Combine(DataPath, "tmp");
        }
        else
        {
            var document = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)!;
            DataPath = Path.Combine(document, AppName);
            LogPath = Path.Combine(DataPath, "logs");
            TempPath = Path.Combine(DataPath, "tmp");
        }

        // builds up configuration
        Config = new ConfigurationBuilder()
            .SetBasePath(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)!)
            .AddJsonFile("AppConfigs.json", optional: false, reloadOnChange: true)
            .AddEnvironmentVariables()
            .AddCommandLine(Environment.GetCommandLineArgs())
            .Build();

        // builds up logging factory, to create logger
        Logging = LoggerFactory.Create(builder => builder
            .SetMinimumLevel(LogLevel.Trace)
            .AddCustomConsoleFormatter(o =>
            {
                o.IncludeScopes = true;
                o.TimestampFormat = "yyyy-MM-dd HH:mm:ss.ffff";
            })
            .AddDebug()
            .AddNLog());

        // setup NLog path
        Environment.SetEnvironmentVariable("NLOG_PATH", LogPath);
        NLog.LogManager.Configuration = new NLogLoggingConfiguration(Config.GetSection("NLog"));

        logger = Logging.CreateLogger<AppBegin>();
    }

    // ----------------------------------------------------------------------

    /// <summary>
    /// Output log for it's information.
    /// </summary>
    public void Print()
    {
        logger?.LogInformation("""
App:

* Portable: {Portable}
* AppPath:  {AppPath}
* DataPath: {DataPath}
* LogPath:  {LogPath}
* TempPath: {TempPath}

""", Portable, AppPath, DataPath, LogPath, TempPath);
    }
}
