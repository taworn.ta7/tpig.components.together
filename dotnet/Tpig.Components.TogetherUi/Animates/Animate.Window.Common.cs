﻿using System;
using System.Windows;
using System.Windows.Media.Animation;

namespace Tpig.Components.TogetherUi;

public static partial class Animate
{
    public static void NoAnimateIn(Window window, Duration timeUsed, EventHandler? handler = null)
    {
        var a = new DoubleAnimation()
        {
            From = 1,
            To = 1,
            Duration = timeUsed
        };
        if (handler != null)
            a.Completed += handler;
        window.BeginAnimation(UIElement.OpacityProperty, a);
    }

    public static void NoAnimateOut(Window window, Duration timeUsed, EventHandler? handler = null)
    {
        var a = new DoubleAnimation()
        {
            From = 1,
            To = 1,
            Duration = timeUsed
        };
        if (handler != null)
            a.Completed += handler;
        window.BeginAnimation(UIElement.OpacityProperty, a);
    }

    // ----------------------------------------------------------------------

    public static void FadeIn(Window window, Duration timeUsed, EventHandler? handler = null)
    {
        var easing = new SineEase() { EasingMode = EasingMode.EaseInOut };
        var a = new DoubleAnimation()
        {
            From = 0,
            To = 1,
            Duration = timeUsed,
            EasingFunction = easing
        };
        if (handler != null)
            a.Completed += handler;
        window.BeginAnimation(UIElement.OpacityProperty, a);
    }

    public static void FadeOut(Window window, Duration timeUsed, EventHandler? handler = null)
    {
        var easing = new SineEase() { EasingMode = EasingMode.EaseInOut };
        var a = new DoubleAnimation()
        {
            From = 1,
            To = 0,
            Duration = timeUsed,
            EasingFunction = easing
        };
        if (handler != null)
            a.Completed += handler;
        window.BeginAnimation(UIElement.OpacityProperty, a);
    }
}
