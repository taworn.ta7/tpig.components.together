﻿using System;
using System.Windows;
using System.Windows.Media.Animation;

namespace Tpig.Components.TogetherUi;

public static partial class Animate
{
    public static void SlideRightToLeftFadeIn(FrameworkElement element, Duration timeUsed, EventHandler? handler = null)
    {
        var easing = new SineEase() { EasingMode = EasingMode.EaseInOut };
        var a = new ThicknessAnimation()
        {
            From = new Thickness(element.ActualWidth, 0, -element.ActualWidth, 0),
            To = new Thickness(0, 0, 0, 0),
            Duration = timeUsed,
            EasingFunction = easing
        };
        var b = new DoubleAnimation()
        {
            From = 0,
            To = 1,
            Duration = timeUsed,
            EasingFunction = easing
        };
        if (handler != null)
            a.Completed += handler;
        element.BeginAnimation(FrameworkElement.MarginProperty, a);
        element.BeginAnimation(UIElement.OpacityProperty, b);
    }

    public static void SlideRightToLeftFadeOut(FrameworkElement element, Duration timeUsed, EventHandler? handler = null)
    {
        var easing = new SineEase() { EasingMode = EasingMode.EaseInOut };
        var a = new ThicknessAnimation()
        {
            From = new Thickness(0, 0, 0, 0),
            To = new Thickness(element.ActualWidth, 0, -element.ActualWidth, 0),
            Duration = timeUsed,
            EasingFunction = easing
        };
        var b = new DoubleAnimation()
        {
            From = 1,
            To = 0,
            Duration = timeUsed,
            EasingFunction = easing
        };
        if (handler != null)
            a.Completed += handler;
        element.BeginAnimation(FrameworkElement.MarginProperty, a);
        element.BeginAnimation(UIElement.OpacityProperty, b);
    }

    // ----------------------------------------------------------------------

    public static void SlideLeftToRightFadeIn(FrameworkElement element, Duration timeUsed, EventHandler? handler = null)
    {
        var easing = new SineEase() { EasingMode = EasingMode.EaseInOut };
        var a = new ThicknessAnimation()
        {
            From = new Thickness(-element.ActualWidth, 0, element.ActualWidth, 0),
            To = new Thickness(0, 0, 0, 0),
            Duration = timeUsed,
            EasingFunction = easing
        };
        var b = new DoubleAnimation()
        {
            From = 0,
            To = 1,
            Duration = timeUsed,
            EasingFunction = easing
        };
        if (handler != null)
            a.Completed += handler;
        element.BeginAnimation(FrameworkElement.MarginProperty, a);
        element.BeginAnimation(UIElement.OpacityProperty, b);
    }

    public static void SlideLeftToRightFadeOut(FrameworkElement element, Duration timeUsed, EventHandler? handler = null)
    {
        var easing = new SineEase() { EasingMode = EasingMode.EaseInOut };
        var a = new ThicknessAnimation()
        {
            From = new Thickness(0, 0, 0, 0),
            To = new Thickness(-element.ActualWidth, 0, element.ActualWidth, 0),
            Duration = timeUsed,
            EasingFunction = easing
        };
        var b = new DoubleAnimation()
        {
            From = 1,
            To = 0,
            Duration = timeUsed,
            EasingFunction = easing
        };
        if (handler != null)
            a.Completed += handler;
        element.BeginAnimation(FrameworkElement.MarginProperty, a);
        element.BeginAnimation(UIElement.OpacityProperty, b);
    }

    // ----------------------------------------------------------------------

    public static void SlideBottomToTopFadeIn(FrameworkElement element, Duration timeUsed, EventHandler? handler = null)
    {
        var easing = new SineEase() { EasingMode = EasingMode.EaseInOut };
        var a = new ThicknessAnimation()
        {
            From = new Thickness(0, element.ActualHeight, 0, -element.ActualHeight),
            To = new Thickness(0, 0, 0, 0),
            Duration = timeUsed,
            EasingFunction = easing
        };
        var b = new DoubleAnimation()
        {
            From = 0,
            To = 1,
            Duration = timeUsed,
            EasingFunction = easing
        };
        if (handler != null)
            a.Completed += handler;
        element.BeginAnimation(FrameworkElement.MarginProperty, a);
        element.BeginAnimation(UIElement.OpacityProperty, b);
    }

    public static void SlideBottomToTopFadeOut(FrameworkElement element, Duration timeUsed, EventHandler? handler = null)
    {
        var easing = new SineEase() { EasingMode = EasingMode.EaseInOut };
        var a = new ThicknessAnimation()
        {
            From = new Thickness(0, 0, 0, 0),
            To = new Thickness(0, element.ActualHeight, 0, -element.ActualHeight),
            Duration = timeUsed,
            EasingFunction = easing
        };
        var b = new DoubleAnimation()
        {
            From = 1,
            To = 0,
            Duration = timeUsed,
            EasingFunction = easing
        };
        if (handler != null)
            a.Completed += handler;
        element.BeginAnimation(FrameworkElement.MarginProperty, a);
        element.BeginAnimation(UIElement.OpacityProperty, b);
    }

    // ----------------------------------------------------------------------

    public static void SlideTopToBottomFadeIn(FrameworkElement element, Duration timeUsed, EventHandler? handler = null)
    {
        var easing = new SineEase() { EasingMode = EasingMode.EaseInOut };
        var a = new ThicknessAnimation()
        {
            From = new Thickness(0, -element.ActualHeight, 0, element.ActualHeight),
            To = new Thickness(0, 0, 0, 0),
            Duration = timeUsed,
            EasingFunction = easing
        };
        var b = new DoubleAnimation()
        {
            From = 0,
            To = 1,
            Duration = timeUsed,
            EasingFunction = easing
        };
        if (handler != null)
            a.Completed += handler;
        element.BeginAnimation(FrameworkElement.MarginProperty, a);
        element.BeginAnimation(UIElement.OpacityProperty, b);
    }

    public static void SlideTopToBottomFadeOut(FrameworkElement element, Duration timeUsed, EventHandler? handler = null)
    {
        var easing = new SineEase() { EasingMode = EasingMode.EaseInOut };
        var a = new ThicknessAnimation()
        {
            From = new Thickness(0, 0, 0, 0),
            To = new Thickness(0, -element.ActualHeight, 0, element.ActualHeight),
            Duration = timeUsed,
            EasingFunction = easing
        };
        var b = new DoubleAnimation()
        {
            From = 1,
            To = 0,
            Duration = timeUsed,
            EasingFunction = easing
        };
        if (handler != null)
            a.Completed += handler;
        element.BeginAnimation(FrameworkElement.MarginProperty, a);
        element.BeginAnimation(UIElement.OpacityProperty, b);
    }
}
