﻿public interface INode
{
    /// <summary>
    /// Call when page is entering.
    /// </summary>
    void Enter(string name);

    /// <summary>
    /// Call when page is leaving.
    /// </summary>
    void Leave(string name);
}
