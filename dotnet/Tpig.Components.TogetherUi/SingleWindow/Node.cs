﻿using System.Windows.Controls;

namespace Tpig.Components.TogetherUi;

/// <summary>
/// A data node which act as way point in Map.
/// </summary>
public class Node
{
    /// <summary>
    /// An unique index, must manage by the Map.
    /// </summary>
    public int Index { get; set; } = -1;

    /// <summary>
    /// A name which reference when call this node.
    /// </summary>
    public string Name { get; set; } = string.Empty;

    /// <summary>
    /// A create UserControl function.
    /// </summary>
    public CreateControlType? CreateControl { get; set; }
    public delegate UserControl CreateControlType();

    /// <summary>
    /// A UserControl page.
    /// </summary>
    public UserControl? Page { get; set; }


    /// <summary>
    /// A default back link.
    /// </summary>
    public Node? BackLink { get; set; }

    /// <summary>
    /// A default next link.
    /// </summary>
    public Node? NextLink { get; set; }


    /// <summary>
    /// Timeout, can be null to use default.
    /// </summary>
    public TimeSpan? TimeOut { get; set; }

    /// <summary>
    /// Time warning to display, can be null to use default.
    /// </summary>
    public TimeSpan? TimeWarn { get; set; }

    /// <summary>
    /// If timeout expired, it will call this function, or leave default to go to first page.
    /// </summary>
    public Action<Node>? Expire { get; set; }


    public override string ToString() =>
        string.Format("{0}: {1}", Index, Name);
}
