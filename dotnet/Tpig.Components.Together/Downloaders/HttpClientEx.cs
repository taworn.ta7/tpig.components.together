﻿using System;
using System.Text;
using System.Threading.Tasks;

namespace Tpig.Components.Together;

public class HttpClientEx : IDisposable
{
    /// <summary>
    /// URL to download.
    /// </summary>
    public string Url { get; private set; }

    /// <summary>
    /// Full path to downloaded file.
    /// </summary>
    public string TargetFile { get; private set; }

    /// <summary>
    /// Total downloading size, in bytes.  Can be null to indicate unknown.
    /// </summary>
    public long? TotalBytes { get; private set; }

    // ----------------------------------------------------------------------

    /// <summary>
    /// Before download event.  This allow user perform HTTP before start download.
    /// </summary>
    public BeforeDownloadDelegate? BeforeDownload;
    public delegate void BeforeDownloadDelegate(HttpClient client);

    /// <summary>
    /// Progressing change.  This may not called due to download file is too small.
    /// </summary>
    public ProgressChangedDelegate? ProgressChanged;
    public delegate void ProgressChangedDelegate(long totalDownloadedBytes, double? percentage);

    /// <summary>
    /// File download completed or error.
    /// </summary>
    public AfterDownloadedDelegate? AfterDownloaded;
    public delegate void AfterDownloadedDelegate(bool success);

    // ----------------------------------------------------------------------

    private HttpClient? client;

    public HttpClientEx(string url, string targetFile)
    {
        Url = url;
        TargetFile = targetFile;
    }

    // ----------------------------------------------------------------------

    public async Task DownloadAsync()
    {
        client = new HttpClient
        {
            Timeout = TimeSpan.FromMinutes(30),
        };
        BeforeDownload?.Invoke(client);

        using (var response = await client.GetAsync(Url, HttpCompletionOption.ResponseHeadersRead))
            await DownloadFileFromHttpResponseMessage(response);
    }

    // ----------------------------------------------------------------------

    private async Task DownloadFileFromHttpResponseMessage(HttpResponseMessage response)
    {
        response.EnsureSuccessStatusCode();

        TotalBytes = response.Content.Headers.ContentLength;

        using (var contentStream = await response.Content.ReadAsStreamAsync())
            await ProcessContentStream(TotalBytes, contentStream);
    }

    private async Task ProcessContentStream(long? totalDownloadSize, Stream contentStream)
    {
        var totalBytesRead = 0L;
        var readCount = 0L;
        var buffer = new byte[16384];
        var isMoreToRead = true;

        using (var fileStream = new FileStream(TargetFile, FileMode.Create, FileAccess.Write, FileShare.None, 16384, true))
        {
            do
            {
                var bytesRead = await contentStream.ReadAsync(buffer, 0, buffer.Length);
                if (bytesRead == 0)
                {
                    isMoreToRead = false;
                    TriggerProgressChanged(totalBytesRead);
                    continue;
                }

                await fileStream.WriteAsync(buffer, 0, bytesRead);

                totalBytesRead += bytesRead;
                readCount += 1;

                if (readCount % 100 == 0)
                    TriggerProgressChanged(totalBytesRead);
            }
            while (isMoreToRead);

            var success = true;
            if (TotalBytes.HasValue)
            {
                if (TotalBytes.Value != totalBytesRead)
                    success = false;
            }
            AfterDownloaded?.Invoke(success);
        }
    }

    private void TriggerProgressChanged(long totalBytesRead)
    {
        if (ProgressChanged == null)
            return;

        double? progressPercentage = null;
        if (TotalBytes.HasValue)
            progressPercentage = Math.Round((double)totalBytesRead / TotalBytes.Value * 100, 2);

        ProgressChanged(totalBytesRead, progressPercentage);
    }

    public void Dispose()
    {
        client?.Dispose();
    }
}
