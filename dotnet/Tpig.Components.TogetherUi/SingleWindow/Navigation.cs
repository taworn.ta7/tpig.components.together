﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using System.Xml.Linq;

namespace Tpig.Components.TogetherUi;

/// <summary>
/// A navigation on Map.
/// </summary>
public partial class Navigation : ISingleWindow
{
    /// <summary>
    /// A parent control.
    /// </summary>
    public Panel Parent { get; private set; }

    /// <summary>
    /// A map to guide navigation.
    /// </summary>
    public Map Map { get; private set; }

    /// <summary>
    /// Current node.
    /// </summary>
    public Node? Node { get; private set; }


    private DispatcherTimer? timer = null;
    private DateTime startTime = DateTime.Now;

    private bool alreadySetup = false;

    // ----------------------------------------------------------------------

    /// <summary>
    /// Constructor.
    /// </summary>
    public Navigation(Panel parent, Map map)
    {
        Parent = parent;
        Map = map;
    }

    // ----------------------------------------------------------------------

    public void Setup()
    {
        if (alreadySetup)
            throw new Exception("Setup already finished.  Cannot setup again!");

        if (Map.List.Count <= 0)
            throw new Exception("Node count is zero!  You have to add Node(s) into Map, first.");
        Node = Map.List[0];

        // loop all Node(s), create all UserControl(s)
        for (var i = 0; i < Map.List.Count; i++)
        {
            var node = Map.List[i];
            var control = node.CreateControl!()
                ?? throw new Exception("Cannot create node!  Every node must have create function.");
            control.Visibility = Visibility.Hidden;
            Parent.Children.Add(control);
            node.Page = control;

            Debug(string.Format("{0}", node));
        }

        // create timer
        timer = new DispatcherTimer(DispatcherPriority.Background)
        {
            Interval = TimeSpan.FromMilliseconds(200),
            IsEnabled = false,
        };
        timer.Tick += Tick!;

        // enter first page
        Node.Page!.Visibility = Visibility.Visible;
        Node.Page!.Focus();
        (Node.Page as INode)?.Enter(Node.Name);
        IsEnabled = true;

        alreadySetup = true;
    }
}
