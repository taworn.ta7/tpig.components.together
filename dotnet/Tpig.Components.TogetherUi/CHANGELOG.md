﻿version 0.2.0:

* BoolToVisibilityConverter removed, use [BooleanToVisibilityConverter](https://learn.microsoft.com/en-us/dotnet/api/system.windows.controls.booleantovisibilityconverter?view=windowsdesktop-8.0)
* delete RelayCommand and BaseViewModel, use [Community Toolkit](https://learn.microsoft.com/en-us/dotnet/communitytoolkit/mvvm) instead
* MessageWindow, use [Nuvola icons](https://commons.wikimedia.org/wiki/Category:Nuvola_icons) from [Icon King](http://www.icon-king.com)
* SingleWindow has revamp and if you use this library, you have to changes, see [Single Window](../Documentation/README-SingleWindow.md)

version 0.1.0:

* initial

