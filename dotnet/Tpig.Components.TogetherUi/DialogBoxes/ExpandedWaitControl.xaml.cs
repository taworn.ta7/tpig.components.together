﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Tpig.Components.TogetherUi;

/// <summary>
/// Interaction logic for ExpandedWaitControl.xaml
/// </summary>
public partial class ExpandedWaitControl : UserControl
{
    private static SequenceAnimation? sprite = null;

    private readonly SequenceAnimationRenderer? renderer = null;
    private readonly TimeSpan animateTime = TimeSpan.FromMilliseconds(300);

    public ExpandedWaitControl()
    {
        InitializeComponent();
        Visibility = Visibility.Hidden;

        if (sprite == null)
        {
            sprite = new SequenceAnimation
            {
                DefaultTimeOut = TimeSpan.FromMilliseconds(50),
                IsSharedImages = true
            };
            var resPathTemplate = "/Tpig.Components.TogetherUi;component/Resources/Loading/frame-{0}.png";
            for (var i = 0; i < 20; i++)
            {
                var number = string.Format("{0:00}", i);
                var resPath = string.Format(resPathTemplate, number);
                var image = new BitmapImage(new Uri(resPath, UriKind.RelativeOrAbsolute));
                sprite.Add(image);
            }
        }

        renderer = new SequenceAnimationRenderer(WaitImage)
        {
            Animation = sprite,
            End = SequenceAnimationRenderer.PlayEnd.Repeat
        };
    }

    /// <summary>
    /// Set Wait to true to open the control, otherwise, set to false to close the control.
    /// </summary>
    private bool wait = false;
    public bool Wait
    {
        get => wait;
        set
        {
            if (wait != value)
            {
                if (value)
                    Begin();
                else
                    End();
            }
        }
    }

    /// <summary>
    /// Open the control.
    /// </summary>
    public void Begin(string? text = null, Color? color = null)
    {
        if (wait)
            return;
        wait = true;

        BackgroundPanel.Background = new SolidColorBrush(color ?? Color.FromArgb(0x88, 0x88, 0x88, 0x88));
        WaitText.Text = text;
        WaitText.Visibility = text != null ? Visibility.Visible : Visibility.Collapsed;
        Visibility = Visibility.Visible;

        renderer?.Play();

        Animate.FadeIn(this, animateTime, (_0, _1) =>
        {
        });
    }

    /// <summary>
    /// Close the control.
    /// </summary>
    public void End(Action? ended = null)
    {
        if (!wait)
            return;

        Animate.FadeOut(this, animateTime, (_0, _1) =>
        {
            renderer?.Stop();

            Visibility = Visibility.Hidden;
            wait = false;
            ended?.Invoke();
        });
    }
}
