﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;

namespace Tpig.Components.TogetherUi;

public static partial class Helpful
{
    /// <summary>
    /// Thank you from https://stackoverflow.com/questions/974598/find-all-controls-in-wpf-window-by-type
    /// and https://stackoverflow.com/users/73509/bryce-kahle.
    /// </summary>
    public static IEnumerable<T> FindVisualChildren<T>(DependencyObject parent) where T : DependencyObject
    {
        if (parent != null)
        {
            for (var i = 0; i < VisualTreeHelper.GetChildrenCount(parent); i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);
                if (child != null && child is T)
                {
                    yield return (T)child;
                }
                foreach (var childOfChild in FindVisualChildren<T>(child!))
                {
                    yield return childOfChild;
                }
            }
        }
    }
}
