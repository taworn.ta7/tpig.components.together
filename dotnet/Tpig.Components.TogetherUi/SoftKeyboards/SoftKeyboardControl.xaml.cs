﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using InputSimulatorStandard;
using Tpig.Components.TogetherUi.KeyboardLayouts;

namespace Tpig.Components.TogetherUi;

/// <summary>
/// Interaction logic for SoftKeyboardControl.xaml
/// </summary>
public partial class SoftKeyboardControl : UserControl
{
    private KeyboardControl? keyboardControl = null;
    private NumericControl? numericControl = null;

    public SoftKeyboardControl()
    {
        InitializeComponent();

        Loaded += (sender, e) =>
        {
            LoadKeyboards();
            Visibility = Visibility.Collapsed;
        };

        Unloaded += (sender, e) =>
        {
        };
    }

    private void LoadKeyboards()
    {
        var i = new InputSimulator();

        {
            var vm = new KeyboardViewModel(i);
            var control = new KeyboardControl(vm);
            Grid.SetRow(control, 2);
            KeyboardPanel.Children.Add(control);
            control.Visibility = Visibility.Collapsed;
            keyboardControl = control;
        }

        {
            var vm = new KeyboardViewModel(i);
            var control = new NumericControl(vm);
            Grid.SetRow(control, 2);
            KeyboardPanel.Children.Add(control);
            control.Visibility = Visibility.Collapsed;
            numericControl = control;
        }
    }

    // ----------------------------------------------------------------------

    private TimeSpan timeAnimation = TimeSpan.FromMilliseconds(150);

    private void AdjustPosition(Control control, UserControl keyboardControl)
    {
        var parent = Window.GetWindow(control);
        var r = new Rect(parent.Left, parent.Top, parent.ActualWidth, parent.ActualHeight);
        var pt = control.TransformToAncestor(parent).Transform(new Point(0, 0));
        if (pt.Y < r.Height / 2)
        {
            Grid.SetRow(keyboardControl, 2);
            Dispatcher.BeginInvoke(DispatcherPriority.Render, (Action)(() => Animate.SlideBottomToTopFadeIn(this, timeAnimation)));
        }
        else
        {
            Grid.SetRow(keyboardControl, 0);
            Dispatcher.BeginInvoke(DispatcherPriority.Render, (Action)(() => Animate.SlideTopToBottomFadeIn(this, timeAnimation)));
        }
    }

    public void Show(Control control, SoftKeyboardEnum type)
    {
        Visibility = Visibility.Visible;

        keyboardControl!.Visibility = Visibility.Collapsed;
        numericControl!.Visibility = Visibility.Collapsed;
        if (type == SoftKeyboardEnum.International)
        {
            keyboardControl.EnglishOnly = false;
            AdjustPosition(control, keyboardControl);
            keyboardControl.Visibility = Visibility.Visible;
        }
        else if (type == SoftKeyboardEnum.EnglishOnly)
        {
            keyboardControl.EnglishOnly = true;
            AdjustPosition(control, keyboardControl);
            keyboardControl.Visibility = Visibility.Visible;
        }
        else if (type == SoftKeyboardEnum.Numeric)
        {
            AdjustPosition(control, numericControl);
            numericControl.Visibility = Visibility.Visible;
        }
    }

    public void Hide()
    {
        Visibility = Visibility.Collapsed;
    }

    // ----------------------------------------------------------------------

    public void AddThaiLanguage()
    {
        keyboardControl!.AddThaiLanguage();
    }
}
