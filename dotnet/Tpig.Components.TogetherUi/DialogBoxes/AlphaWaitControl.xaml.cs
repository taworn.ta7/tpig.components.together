﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Tpig.Components.TogetherUi;

/// <summary>
/// Interaction logic for AlphaWaitControl.xaml
/// </summary>
public partial class AlphaWaitControl : UserControl
{
    public AlphaWaitControl()
    {
        InitializeComponent();
        Visibility = Visibility.Hidden;
    }

    /// <summary>
    /// Set Wait to true to open the control, otherwise, set to false to close the control.
    /// </summary>
    private bool wait = false;
    public bool Wait
    {
        get => wait;
        set
        {
            if (wait != value)
            {
                if (value)
                    Begin();
                else
                    End();
            }
        }
    }

    /// <summary>
    /// Open the control.
    /// </summary>
    public void Begin(Color? color = null)
    {
        if (wait)
            return;
        wait = true;

        BackgroundPanel.Background = new SolidColorBrush(color ?? Color.FromArgb(0x00, 0x00, 0x00, 0x00));

        Visibility = Visibility.Visible;
    }

    /// <summary>
    /// Close the control.
    /// </summary>
    public void End()
    {
        if (!wait)
            return;
        wait = false;

        Visibility = Visibility.Hidden;
    }
}
