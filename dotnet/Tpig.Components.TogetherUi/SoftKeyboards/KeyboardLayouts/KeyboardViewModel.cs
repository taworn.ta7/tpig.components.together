﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using InputSimulatorStandard;

namespace Tpig.Components.TogetherUi.KeyboardLayouts;

public partial class KeyboardViewModel : ObservableObject, INotifyPropertyChanged
{
    public InputSimulator? Input { get; private set; }

    public KeyboardViewModel(InputSimulator? i) => Input = i;

    // ----------------------------------------------------------------------

    // normal key
    [RelayCommand]
    private void NormalKey(object o) => NormalKeyDelegate?.Invoke(o);
    public Action<object>? NormalKeyDelegate { get; set; } = null;

    // special key
    [RelayCommand]
    private void SpecialKey(object o) => SpecialKeyDelegate?.Invoke(o);
    public Action<object>? SpecialKeyDelegate { get; set; } = null;
}
