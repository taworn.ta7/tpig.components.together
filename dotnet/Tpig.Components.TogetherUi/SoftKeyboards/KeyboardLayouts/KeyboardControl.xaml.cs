﻿using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Input;
using InputSimulatorStandard.Native;

namespace Tpig.Components.TogetherUi.KeyboardLayouts;

/// <summary>
/// Interaction logic for KeyboardControl.xaml
/// </summary>
public partial class KeyboardControl : UserControl
{
    private KeyboardViewModel? viewModel = null;

    public KeyboardControl(KeyboardViewModel vm)
    {
        InitializeComponent();

        Loaded += (sender, e) =>
        {
            viewModel = vm;
            viewModel.NormalKeyDelegate += NormalKeyDelegate;
            viewModel.SpecialKeyDelegate += SpecialKeyDelegate;
            DataContext = viewModel;
        };
        Unloaded += (sender, e) =>
        {
            DataContext = null;
            //viewModel.SpecialKeyDelegate -= SpecialKeyDelegate;
            //viewModel.NormalKeyDelegate -= NormalKeyDelegate;
            viewModel = null;
        };

        ShiftState = false;

        AddEnglishLanguage();
        currentLanguage = "en";
        currentMapping = keyboardMapping[currentLanguage];
        ChangeLanguageTo(currentLanguage);
    }

    // ----------------------------------------------------------------------

    private void NormalKeyDelegate(object o)
    {
        var s = o as string;
        var mapped = Mapping(s);
        //Console.WriteLine("soft key: {0} = {1}", s, mapped);
        viewModel?.Input?.Keyboard.TextEntry(mapped);
        if (shiftKey)
        {
            ShiftState = !ShiftState;
            shiftKey = false;
        }
    }

    private void SpecialKeyDelegate(object o)
    {
        var s = o as string;
        //Console.WriteLine("special key: {0}", s);
        if (s == "CapsLock")
        {
            ShiftState = !ShiftState;
        }
        else if (o.Equals("Shift"))
        {
            shiftKey = !shiftKey;
            ShiftState = !ShiftState;
        }
        else if (s == "Backspace")
        {
            viewModel?.Input?.Keyboard.KeyPress(VirtualKeyCode.BACK);
        }
        else if (s == "Hide")
        {
            var kb = SoftKeyboardUtilities.FindSoftKeyboard(this);
            kb?.Hide();
        }
        else if (s == "Enter")
        {
            var control = Keyboard.FocusedElement as Control;
            control?.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
        }
        else if (s == "ChangeLanguage")
        {
            if (!EnglishOnly)
                ChangeLanguageTo();
        }
    }

    private bool shiftState = false;
    public bool ShiftState
    {
        get => shiftState;
        set
        {
            shiftState = value;
            ShiftPanel.Visibility = shiftState ? System.Windows.Visibility.Visible : System.Windows.Visibility.Hidden;
        }
    }

    private bool shiftKey = false;

    // ----------------------------------------------------------------------

    private class KeyboardMapping
    {
        public string? Name { get; set; }
        public Dictionary<char, char>? Mapping { get; set; }
    }
    private readonly Dictionary<string, KeyboardMapping> keyboardMapping = new();
    private readonly List<string> keyboardList = new();

    private string currentLanguage;
    private KeyboardMapping currentMapping;

    private string Mapping(string? s)
    {
        var o = "";
        var m = currentMapping.Mapping!;
        if (s != null)
        {
            foreach (var c in s)
            {
                if (m.TryGetValue(c, out char v))
                    o += v;
                else
                    o += c;
            }
        }
        return o;
    }

    private void ChangeLanguageTo()
    {
        var i = keyboardList.FindIndex((s) => s == currentLanguage);
        if (i >= 0 && i < keyboardList.Count)
        {
            if (i >= 0 && i < keyboardList.Count - 1)
                i++;
            else
                i = 0;
            currentLanguage = keyboardList[i];
            ChangeLanguageTo(currentLanguage);
        }
    }

    private void ChangeLanguageTo(string target)
    {
        currentMapping = keyboardMapping[target];
        ChangeLanguageButton.Content = currentMapping.Name;

        var style = FindResource("SpecialKey");
        foreach (var item in Helpful.FindVisualChildren<Button>(this))
        {
            if (!item.Style.Equals(style))
            {
                item.Content = item.CommandParameter;
                item.Content = Mapping(item.Content as string);
            }
        }
    }

    private bool englishOnly = false;
    public bool EnglishOnly
    {
        get => englishOnly;
        set
        {
            if (englishOnly != value)
            {
                englishOnly = value;
                currentLanguage = keyboardList[0];
                ChangeLanguageTo(currentLanguage);
            }
        }
    }

    // ----------------------------------------------------------------------

    private void AddEnglishLanguage()
    {
        keyboardMapping.Add("en", new KeyboardMapping
        {
            Name = "ENG",
            Mapping = new Dictionary<char, char>()
        });
        keyboardList.Add("en");
    }
}
