﻿using System.Collections.Generic;

namespace Tpig.Components.TogetherUi;

/// <summary>
/// Data structure to manage Node(s).
/// </summary>
public class Map
{
    /// <summary>
    /// A list data to keep node(s).
    /// </summary>
    public List<Node> List { get; private set; } = new();

    /// <summary>
    /// Name to node lookup data structure.
    /// </summary>
    private readonly Dictionary<string, Node> dict = new();


    /// <summary>
    /// Add new node.
    /// </summary>
    public void Add(
        string name,
        Node.CreateControlType create,
        string? back = null)
    {
        // check if this 'name' must not be found
        var found = dict.TryGetValue(name, out var control);
        if (found)
        {
            throw new ArgumentException(string.Format("Name '{0}' is already used!", name));
        }

        // compute back link
        var backLink = null as Node;
        if (string.IsNullOrWhiteSpace(back))
            backLink = List.Count > 0 ? List[^1] : null;
        else
            backLink = Find(back);

        // create node
        var node = new Node()
        {
            Index = List.Count,
            Name = name,
            CreateControl = create,
            Page = null,
            BackLink = backLink,
            NextLink = null,
            TimeOut = null,
            TimeWarn = null,
            Expire = null,
        };
        if (node.BackLink != null)
        {
            // link node with previous node
            if (node.BackLink.NextLink == null)
                node.BackLink.NextLink = node;
        }

        // add to list and dictionary
        List.Add(node);
        dict.Add(node.Name, node);
    }


    /// <summary>
    /// Find node by name.
    /// </summary>
    public Node? Find(string name)
    {
        dict.TryGetValue(name, out var node);
        return node;
    }
}
