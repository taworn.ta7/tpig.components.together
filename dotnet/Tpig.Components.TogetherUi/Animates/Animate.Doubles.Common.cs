﻿using System;
using System.Windows;
using System.Windows.Media.Animation;

namespace Tpig.Components.TogetherUi;

public static partial class Animate
{
    public static void NoAnimate(
        FrameworkElement element0, FrameworkElement element1, Duration timeUsed,
        EventHandler? handler0 = null, EventHandler? handler1 = null)
    {
        var a = new DoubleAnimation()
        {
            From = 0,
            To = 0,
            Duration = timeUsed
        };
        var b = new DoubleAnimation()
        {
            From = 1,
            To = 1,
            Duration = timeUsed
        };
        if (handler0 != null)
            a.Completed += handler0;
        if (handler1 != null)
            b.Completed += handler1;
        element0.BeginAnimation(UIElement.OpacityProperty, a);
        element1.BeginAnimation(UIElement.OpacityProperty, b);
    }

    // ----------------------------------------------------------------------

    public static void Fade(
        FrameworkElement element0, FrameworkElement element1, Duration timeUsed,
        EventHandler? handler0 = null, EventHandler? handler1 = null)
    {
        var a = new DoubleAnimation()
        {
            From = 1,
            To = 0,
            Duration = timeUsed
        };
        var b = new DoubleAnimation()
        {
            From = 0,
            To = 1,
            Duration = timeUsed
        };
        if (handler0 != null)
            a.Completed += handler0;
        if (handler1 != null)
            b.Completed += handler1;
        element0.BeginAnimation(UIElement.OpacityProperty, a);
        element1.BeginAnimation(UIElement.OpacityProperty, b);
    }
}
