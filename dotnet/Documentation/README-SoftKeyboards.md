## Soft Keyboards

.NET WPF component for virtual keyboards.

First, add this line in xaml:

	xmlns:kb="clr-namespace:Tpig.Components.TogetherUi;assembly=Tpig.Components.TogetherUi"

Then, put this line to instance soft-keyboard, this must be top:

    <kb:SoftKeyboardControl x:Name="SoftKeyboardControl"/>

Then, there are 2 ways to attach soft-keyboards:

- use attach property, which is simple but less configure
- use behavior

When you want to use attach property, here:

	<TextBox kb:AutoSoftKeyboardBehavior.UseSoftKeyboard="True"/>

If you want to use behavior, add this line:

	xmlns:be="http://schemas.microsoft.com/xaml/behaviors"

Then:

	<TextBox>
		<be:Interaction.Behaviors>
			<kb:SoftKeyboardBehavior SoftKeyboardType="International"/>
		</be:Interaction.Behaviors>
	</TextBox>

You can set SoftKeyboardType as International, EnglishOnly and Numeric.

You can add language, like this:

	Loaded += (sender, e) => {
		Dispatcher.BeginInvoke(DispatcherPriority.Normal, (Action)(() => {
			SoftKeyboardControl.AddThaiLanguage();
		}));
	};

Sorry, I can use English and Thai only.  If you want another language, you have to fork.
See SoftKeyboardControl.AddThaiLanguage() as basis.

Please see demo about soft keyboards in Tpig.Demonstrates.SoftKeyboards.

![Screenshot 1](../Screenshots/SoftKeyboards-0.png) | ![Screenshot 2](../Screenshots/SoftKeyboards-1.png)
