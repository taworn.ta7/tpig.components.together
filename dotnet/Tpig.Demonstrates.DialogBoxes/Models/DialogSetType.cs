﻿using System;
using System.Collections.Generic;
using System.Text;
using Tpig.Components.TogetherUi;

namespace Tpig.Demonstrates.DialogBoxes.Models;

public class DialogSetType
{
    public MessageViewModel.DialogSet Id { get; set; }

    public string Name { get => Id.ToString(); }
}
