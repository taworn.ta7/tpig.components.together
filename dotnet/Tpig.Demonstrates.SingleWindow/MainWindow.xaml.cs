﻿using System.ComponentModel;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Tpig.Components.Localization;
using Tpig.Components.TogetherUi;

namespace Tpig.Demonstrates.SingleWindow;

/// <summary>
/// Interaction logic for MainWindow.xaml
/// </summary>
public partial class MainWindow : Window
{
    private MainViewModel? viewModel;

    private Navigation? navigation = null;

    public MainWindow()
    {
        InitializeComponent();

        LocaleManager.Instance.Current = "en";
        Localize.Load(this);

        SetupUi();

        Loaded += (sender, e) =>
        {
            viewModel = new MainViewModel();
            viewModel!.ChangeLocaleDelegate += ChangeLocaleDelegate;
            DataContext = viewModel;
        };

        Unloaded += (sender, e) =>
        {
            DataContext = null;
            viewModel!.ChangeLocaleDelegate -= ChangeLocaleDelegate;
            viewModel = null;
        };
    }

    // ----------------------------------------------------------------------

    private void ChangeLocaleDelegate(object o)
    {
        var locale = o as string;
        LocaleManager.Instance.Current = locale!;
    }

    // ----------------------------------------------------------------------

    private static void AddLog(string level, string log)
    {
        Console.WriteLine("{0:yyyy-MM-dd HH:mm:ss.fff} {1}  {2}", DateTime.Now, level, log);
    }

    // ----------------------------------------------------------------------

    private void Navigating(Node prev, Node next)
    {
        navigation!.Trace(string.Format("navigating: {0} => {1}", prev.Name, next.Name));
    }

    private void Navigated(Node prev, Node next)
    {
        navigation!.Trace(string.Format("navigated: {0} => {1}", prev.Name, next.Name));
    }

    private void TimeLeft(TimeSpan? time)
    {
        if (time != null)
        {
            var format = TimeWarnText.Tag as string;
            TimeWarnText.Text = string.Format(format ?? "{0}", (int)time.Value.TotalSeconds);
        }
        else
        {
            TimeWarnText.Text = "";
        }
    }

    // ----------------------------------------------------------------------

    private void SetupUi()
    {
        // create navigation map
        navigation = new Navigation(Container, new Map());

        // build UI tree
        navigation.Map.Add("First", () => new Pages.FirstPage(navigation));
        navigation.Map.Add("Second", () => new Pages.SecondPage(navigation));
        navigation.Map.Add("Third", () => new Pages.ThirdPage(navigation));
        navigation.Map.Add("Fourth", () => new Pages.FourthPage(navigation), back: "Second");

        // add some customization
        navigation.AnimationBack = Animate.SlideLeftToRightFade;
        navigation.AnimationNext = Animate.SlideBottomToTopFade;
        navigation.Navigating += Navigating;
        navigation.Navigated += Navigated;
        navigation.TimeLeft += TimeLeft;
        navigation.DefaultTimeOut = TimeSpan.FromSeconds(15);
        navigation.DefaultTimeWarn = TimeSpan.FromSeconds(10);
        navigation.TraceDelegate += (log) => AddLog("TRACE", log);
        navigation.DebugDelegate += (log) => AddLog("DEBUG", log);
        navigation.WarningDelegate += (log) => AddLog(" WARN", log);

        // finally, call Setup, and that all :)
        navigation.Setup();
    }
}
