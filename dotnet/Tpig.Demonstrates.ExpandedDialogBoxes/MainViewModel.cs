﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using Tpig.Components.TogetherUi;

namespace Tpig.Demonstrates.ExpandedDialogBoxes;

public partial class MainViewModel : ObservableObject, INotifyPropertyChanged
{
    // change locale
    [RelayCommand]
    private void ChangeLocale(object o) => ChangeLocaleDelegate?.Invoke(o);
    public Action<object>? ChangeLocaleDelegate { get; set; }

    // message
    [RelayCommand]
    private void Message(object o) => MessageDelegate?.Invoke(o);
    public Action<object>? MessageDelegate { get; set; }

    // wait
    [RelayCommand]
    private void Wait(object o) => WaitDelegate?.Invoke(o);
    public Action<object>? WaitDelegate { get; set; } = null;


    // DialogSet
    private List<Models.DialogSetType> dialogSetList = [
        new Models.DialogSetType { Id = MessageViewModel.DialogSet.NormalSet },
        new Models.DialogSetType { Id = MessageViewModel.DialogSet.OkSet },
        new Models.DialogSetType { Id = MessageViewModel.DialogSet.OkCancelSet },
        new Models.DialogSetType { Id = MessageViewModel.DialogSet.YesNoSet },
        new Models.DialogSetType { Id = MessageViewModel.DialogSet.RetrySet },
        new Models.DialogSetType { Id = MessageViewModel.DialogSet.PassSet },
        new Models.DialogSetType { Id = MessageViewModel.DialogSet.InformationSet },
        new Models.DialogSetType { Id = MessageViewModel.DialogSet.QuestionSet },
        new Models.DialogSetType { Id = MessageViewModel.DialogSet.WarningSet },
        new Models.DialogSetType { Id = MessageViewModel.DialogSet.ErrorSet },
    ];
    public List<Models.DialogSetType> DialogSetList
    {
        get => dialogSetList;
    }

    public Models.DialogSetType? DialogSetSelected { get; set; }
}
