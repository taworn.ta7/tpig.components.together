﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace Tpig.Components.TogetherUi;

public partial class Navigation : ISingleWindow
{
    public delegate void AnimationType(FrameworkElement element0, FrameworkElement element1, Duration timeUsed, EventHandler? handler0, EventHandler? handler1);

    /// <summary>
    /// A back animations.
    /// </summary>
    public AnimationType AnimationBack { get; set; } = Animate.SlideLeftToRightFade;

    /// <summary>
    /// A next animations.
    /// </summary>
    public AnimationType AnimationNext { get; set; } = Animate.SlideRightToLeftFade;


    /// <summary>
    /// Direction which have Default, Back and Next.
    /// </summary>
    public enum Direction
    {
        Default,
        Back,
        Next
    }


    /// <summary>
    /// Before navigation event.
    /// Parameter 1 is node to leave.
    /// Parameter 2 is node to enter.
    /// </summary>
    public Action<Node, Node>? Navigating { get; set; }

    /// <summary>
    /// After navigation event.
    /// Parameter 1 is node to leave.
    /// Parameter 2 is node to enter.
    /// </summary>
    public Action<Node, Node>? Navigated { get; set; }

    // ----------------------------------------------------------------------

    /// <summary>
    /// Perform navigation.
    /// </summary>
    public void Navigate(Node node, Direction dir = Direction.Default)
    {
        if (node != null && node != Node)
        {
            if (Node != null)
            {
                // get current node and prepare next node
                var element0 = Node.Page!;
                var element1 = node.Page!;
                element1.Visibility = Visibility.Visible;

                // check animation to direction, back for next
                var animation = (AnimationType?)null;
                switch (dir)
                {
                    default:
                    case Direction.Default:
                        if (Node.Index < node.Index)
                            animation = AnimationNext;
                        else
                            animation = AnimationBack;
                        break;
                    case Direction.Back:
                        animation = AnimationBack;
                        break;
                    case Direction.Next:
                        animation = AnimationNext;
                        break;
                }

                var prevNode = Node;
                var nextNode = node;

                IsEnabled = false;
                Navigating?.Invoke(prevNode, nextNode);
                (element0 as INode)?.Leave(Node.Name);
                Parent.Dispatcher.BeginInvoke(DispatcherPriority.Render, (Action)(() =>
                {
                    animation(element0, element1, TimeSpan.FromMilliseconds(150),
                        (_0, _1) => element0.Visibility = Visibility.Hidden,
                        (_0, _1) =>
                        {
                            Node = node;
                            element1.Focus();
                            (element1 as INode)?.Enter(Node.Name);
                            Navigated?.Invoke(prevNode, nextNode);
                            IsEnabled = true;
                        });
                }));
            }
        }
    }

    // ----------------------------------------------------------------------

    /// <summary>
    /// Move to back.
    /// </summary>
    public Node? Back(string? name)
    {
        Node? link;
        if (string.IsNullOrWhiteSpace(name))
            link = Node!.BackLink;
        else
            link = Map.Find(name);

        if (link != null)
        {
            Navigate(link, Direction.Back);
            return link;
        }
        else
            return null;
    }

    /// <summary>
    /// Move to next.
    /// </summary>
    public Node? Next(string? name)
    {
        Node? link;
        if (string.IsNullOrWhiteSpace(name))
            link = Node!.NextLink;
        else
            link = Map.Find(name);

        if (link != null)
        {
            Navigate(link, Direction.Next);
            return link;
        }
        else
            return null;
    }
}
