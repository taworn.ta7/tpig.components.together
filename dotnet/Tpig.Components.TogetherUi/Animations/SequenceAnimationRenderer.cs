﻿using System.Threading;
using System.Windows.Controls;

namespace Tpig.Components.TogetherUi;

/// <summary>
/// A simple animation renderer.
/// </summary>
public class SequenceAnimationRenderer
{
    // image control to output
    public Image? DisplayImage { get; private set; } = null;

    // source image list
    public SequenceAnimation Animation { get; set; } = new SequenceAnimation();

    // current frame
    public int CurrentIndex { get; private set; } = 0;

    // when play end?
    public enum PlayEnd
    {
        Stop,
        Repeat
    }
    public PlayEnd End { get; set; } = PlayEnd.Repeat;

    // is playing
    public bool IsPlaying { get; private set; } = false;

    // timer
    private Timer? timer = null;

    public SequenceAnimationRenderer(Image image)
    {
        DisplayImage = image;
        timer = new Timer((o) => Next(), null, Timeout.Infinite, Timeout.Infinite);
    }

    private void Next()
    {
        if (DisplayImage != null)
            DisplayImage.Dispatcher.Invoke(() =>
            {
                if (IsPlaying)
                {
                    if (Animation != null && Animation.Count > 0)
                    {
                        var image = Animation.Get(CurrentIndex++);
                        if (image != null)
                        {
                            DisplayImage.Source = image;
                            if (CurrentIndex >= Animation.Count)
                            {
                                CurrentIndex = 0;
                                if (End == PlayEnd.Stop)
                                {
                                    Stop();
                                    return;
                                }
                            }
                            var timeOut = Animation.GetTimeOut(CurrentIndex).TotalMilliseconds;
                            timer!.Change((int)timeOut, Timeout.Infinite);
                        }
                    }
                }
            });
    }

    public void Play(bool restart = false)
    {
        if (Animation != null)
        {
            IsPlaying = true;
            if (restart)
                CurrentIndex = 0;
            if (CurrentIndex >= 0 && CurrentIndex < Animation.Count)
                Next();
        }
    }

    public void Stop()
    {
        timer!.Change(Timeout.Infinite, Timeout.Infinite);
        IsPlaying = false;
    }
}
