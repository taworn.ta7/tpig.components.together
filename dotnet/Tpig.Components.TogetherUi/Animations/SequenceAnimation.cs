﻿using System;
using System.Collections.Generic;
using System.Windows.Media;

namespace Tpig.Components.TogetherUi;

/// <summary>
/// A simple traditional animation.
/// </summary>
public class SequenceAnimation
{
    /// <summary>
    /// Single entry.
    /// </summary>
    public class Entry
    {
        public ImageSource? Image { get; set; } = null;
        public TimeSpan? TimeOut { get; set; } = null;
    }

    // image list
    public List<Entry> List { get; private set; } = new List<Entry>();

    // image count, shortcut to list.Count
    public int Count { get => List.Count; }

    // default time in frame
    public TimeSpan DefaultTimeOut { get; set; } = TimeSpan.FromMilliseconds(1);

    // can freeze (false) or not (true)
    public bool IsSharedImages { get; set; } = false;

    // constructor
    public SequenceAnimation()
    {
    }

    /// <summary>
    /// Clear image list.
    /// </summary>
    public void Clear()
    {
        List.Clear();
        List = new List<Entry>();
    }

    /// <summary>
    /// Add an image into image list.
    /// </summary>
    public void Add(ImageSource image, TimeSpan? timeOut = null)
    {
        List.Add(new Entry
        {
            Image = image,
            TimeOut = timeOut
        });
        if (!IsSharedImages)
        {
            if (image.CanFreeze)
                image.Freeze();
        }
    }

    /// <summary>
    /// Get an image by index.
    /// </summary>
    public ImageSource? Get(int index)
    {
        if (index >= 0 && index < List.Count)
            return List[index].Image;
        else
            return null;
    }

    /// <summary>
    /// Get timeout by index.
    /// </summary>
    public TimeSpan GetTimeOut(int index)
    {
        if (index >= 0 && index < List.Count)
            return List[index].TimeOut ?? DefaultTimeOut;
        else
            return DefaultTimeOut;
    }
}
