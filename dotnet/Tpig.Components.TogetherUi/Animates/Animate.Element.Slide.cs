﻿using System;
using System.Windows;
using System.Windows.Media.Animation;

namespace Tpig.Components.TogetherUi;

public static partial class Animate
{
    public static void SlideRightToLeftIn(FrameworkElement element, Duration timeUsed, EventHandler? handler = null)
    {
        var easing = new SineEase() { EasingMode = EasingMode.EaseInOut };
        var a = new ThicknessAnimation()
        {
            From = new Thickness(element.ActualWidth, 0, -element.ActualWidth, 0),
            To = new Thickness(0, 0, 0, 0),
            Duration = timeUsed,
            EasingFunction = easing
        };
        if (handler != null)
            a.Completed += handler;
        element.BeginAnimation(FrameworkElement.MarginProperty, a);
    }

    public static void SlideRightToLeftOut(FrameworkElement element, Duration timeUsed, EventHandler? handler = null)
    {
        var easing = new SineEase() { EasingMode = EasingMode.EaseInOut };
        var a = new ThicknessAnimation()
        {
            From = new Thickness(0, 0, 0, 0),
            To = new Thickness(element.ActualWidth, 0, -element.ActualWidth, 0),
            Duration = timeUsed,
            EasingFunction = easing
        };
        if (handler != null)
            a.Completed += handler;
        element.BeginAnimation(FrameworkElement.MarginProperty, a);
    }

    // ----------------------------------------------------------------------

    public static void SlideLeftToRightIn(FrameworkElement element, Duration timeUsed, EventHandler? handler = null)
    {
        var easing = new SineEase() { EasingMode = EasingMode.EaseInOut };
        var a = new ThicknessAnimation()
        {
            From = new Thickness(-element.ActualWidth, 0, element.ActualWidth, 0),
            To = new Thickness(0, 0, 0, 0),
            Duration = timeUsed,
            EasingFunction = easing
        };
        if (handler != null)
            a.Completed += handler;
        element.BeginAnimation(FrameworkElement.MarginProperty, a);
    }

    public static void SlideLeftToRightOut(FrameworkElement element, Duration timeUsed, EventHandler? handler = null)
    {
        var easing = new SineEase() { EasingMode = EasingMode.EaseInOut };
        var a = new ThicknessAnimation()
        {
            From = new Thickness(0, 0, 0, 0),
            To = new Thickness(-element.ActualWidth, 0, element.ActualWidth, 0),
            Duration = timeUsed,
            EasingFunction = easing
        };
        if (handler != null)
            a.Completed += handler;
        element.BeginAnimation(FrameworkElement.MarginProperty, a);
    }

    // ----------------------------------------------------------------------

    public static void SlideBottomToTopIn(FrameworkElement element, Duration timeUsed, EventHandler? handler = null)
    {
        var easing = new SineEase() { EasingMode = EasingMode.EaseInOut };
        var a = new ThicknessAnimation()
        {
            From = new Thickness(0, element.ActualHeight, 0, -element.ActualHeight),
            To = new Thickness(0, 0, 0, 0),
            Duration = timeUsed,
            EasingFunction = easing
        };
        if (handler != null)
            a.Completed += handler;
        element.BeginAnimation(FrameworkElement.MarginProperty, a);
    }

    public static void SlideBottomToTopOut(FrameworkElement element, Duration timeUsed, EventHandler? handler = null)
    {
        var easing = new SineEase() { EasingMode = EasingMode.EaseInOut };
        var a = new ThicknessAnimation()
        {
            From = new Thickness(0, 0, 0, 0),
            To = new Thickness(0, element.ActualHeight, 0, -element.ActualHeight),
            Duration = timeUsed,
            EasingFunction = easing
        };
        if (handler != null)
            a.Completed += handler;
        element.BeginAnimation(FrameworkElement.MarginProperty, a);
    }

    // ----------------------------------------------------------------------

    public static void SlideTopToBottomIn(FrameworkElement element, Duration timeUsed, EventHandler? handler = null)
    {
        var easing = new SineEase() { EasingMode = EasingMode.EaseInOut };
        var a = new ThicknessAnimation()
        {
            From = new Thickness(0, -element.ActualHeight, 0, element.ActualHeight),
            To = new Thickness(0, 0, 0, 0),
            Duration = timeUsed,
            EasingFunction = easing
        };
        if (handler != null)
            a.Completed += handler;
        element.BeginAnimation(FrameworkElement.MarginProperty, a);
    }

    public static void SlideTopToBottomOut(FrameworkElement element, Duration timeUsed, EventHandler? handler = null)
    {
        var easing = new SineEase() { EasingMode = EasingMode.EaseInOut };
        var a = new ThicknessAnimation()
        {
            From = new Thickness(0, 0, 0, 0),
            To = new Thickness(0, -element.ActualHeight, 0, element.ActualHeight),
            Duration = timeUsed,
            EasingFunction = easing
        };
        if (handler != null)
            a.Completed += handler;
        element.BeginAnimation(FrameworkElement.MarginProperty, a);
    }
}
